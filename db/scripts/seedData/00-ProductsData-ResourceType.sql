SET QUOTED_IDENTIFIER ON
GO
INSERT INTO Products.ResourceType(Name,Description, CreatedBy) VALUES('PDF', 'Acrobat PDF', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
INSERT INTO Products.ResourceType(Name,Description, CreatedBy) VALUES('HTML', 'HTML Link', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
INSERT INTO Products.ResourceType(Name,Description, CreatedBy) VALUES('YouTube', 'YouTube Link', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
INSERT INTO Products.ResourceType(Name,Description, CreatedBy) VALUES('Vuforia', 'Vuforia Link', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
