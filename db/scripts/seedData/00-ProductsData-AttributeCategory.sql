SET QUOTED_IDENTIFIER ON
GO
INSERT INTO Products.AttributeCategory
    (Name)
    Values('Bike Geometry'),
    ('Bike Fit'),
    ('Tire'),
    ('Odometer'),
    ('Speed Sensor'),
    ('Suspension Setup'),
    ('Travel')