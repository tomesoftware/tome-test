SET QUOTED_IDENTIFIER ON
GO
INSERT INTO Products.Status(Name, CreatedBy) VALUES('Active', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
INSERT INTO Products.Status(Name, CreatedBy) VALUES('Retired', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
INSERT INTO Products.Status(Name, CreatedBy) VALUES('Sold', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
INSERT INTO Products.Status(Name, CreatedBy) VALUES('Stolen', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = 'SAP' AND r.Name = 'System'));
