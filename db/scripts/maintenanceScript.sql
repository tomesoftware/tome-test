insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Mechanical Safety and Fit Check')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name NOT IN (
				'')
			)
		);

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Mechanical Safety Check')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name NOT IN (
				'')
			)
		)

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Break in adjustment' AND Description = 'After 5 hours')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Mountain', 'e-MTB'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Clean bike and Lube chain' AND Description = 'Every 5 hours')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Mountain', 'e-MTB'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Safety Inspection' AND Description = 'Every 10 hours')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Mountain', 'e-MTB'
			)
		))
		
		
insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Preventative Maintenance Service' AND Description = 'Every 25 hours or Annually')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Mountain', 'e-MTB'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Full Service' AND Description = 'Every 50 hours or Bi-Annually')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Mountain', 'e-MTB'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Break in adjustment' AND Description = 'After 10 hours')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Road', 'Urban', 'Kids''', 'E-Road', 'E-Urban'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Clean bike and Lube chain' AND Description = 'After 10 hours')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Road', 'Urban', 'Kids''', 'E-Road', 'E-Urban'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Safety Inspection' AND Description = 'Every 20 hours')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Road', 'Urban', 'Kids''', 'E-Road', 'E-Urban'
			)
		))

insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Preventative Maintenance Service' AND Description = 'Every 50 hours or Annually')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Road', 'Urban', 'Kids''', 'E-Road', 'E-Urban'
			)
		))
		
insert into Products.ModelMaintenance (ModelId, MaintenanceId) 
	SELECT ModelId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Full Service' AND Description = 'Every 100 hours or Bi-Annually')
	FROM Products.Model where PlatformId IN 
		(SELECT PlatformId FROM Products.Platform WHERE PlatformTypeId IN (
			SELECT PlatformTypeId from Products.PlatformType WHERE Name IN (
				'Road', 'Urban', 'Kids''', 'E-Road', 'E-Urban'
			)
		))
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Check Fastener Torque' AND Description = 'Before first ride')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) LIKE (
				'%LEFTY%'
			)
		))

insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Damage Inspection' AND Description = 'Before every ride')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) LIKE (
				'%LEFTY%'
			)
		))
		
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Check Fastener Torque' AND Description = 'Every 4 to 5 rides')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) LIKE (
				'%LEFTY%'
			)
		))
		
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = '100 Hour Service' AND Description = 'Every 100 hours or Annually')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) LIKE (
				'%LEFTY%'
			)
		))
		
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Full Service' AND Description = 'Every 200 hours or Bi-Annually')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) LIKE (
				'%LEFTY%'
			)
		))
		
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Damage Inspection' AND Description = 'Before every ride')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) NOT LIKE (
				'%LEFTY%'
			)
		))
		
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Lower Leg Service' AND Description = 'Every 50 hours or Annually')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) NOT LIKE (
				'%LEFTY%'
			)
		))
		
insert into Products.PartMaintenance (PartId, MaintenanceId) 
	SELECT PartId, (SELECT MaintenanceId FROM Pawl.Products.Maintenance WHERE Name = 'Damper and Spring Service' AND Description = 'Every 200 hours or Bi-Annually')
	FROM Products.Part where PartId IN 
		(SELECT PartId FROM Products.SuspensionTypePart WHERE SuspensionTypeId IN (
			SELECT SuspensionTypeId from Pawl.Products.SuspensionType WHERE UPPER(Name) NOT LIKE (
				'%LEFTY%'
			)
		))