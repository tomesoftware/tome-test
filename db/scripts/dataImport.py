# import pyodbc
import os.path
import csv
import pyodbc

data_folder = 'seedData'

user='sa'
password='PawlSimon123'
database='master'
port='1433'
TDS_Version='8.0'
server='localhost'
driver='FreeTDS'
con_string='UID=%s;PWD=%s;DATABASE=%s;PORT=%s;TDS=%s;SERVER=%s;driver=%s' % (user,password, database,port,TDS_Version,server,driver)
# cnxn=pyodbc.connect(con_string)
# cursor=cnxn.cursor()
dataFiles = ['Attribute.csv']
#dataFiles = ['Status.csv', 'Manufacturer.csv', 'PlatformType.csv', 'Platform.csv',
#    'Model.csv', 'Material.csv', 'PartType.csv', 'Part.csv', 'MaterialBOM.csv', 'MaterialAttribute.csv', 'MFDPart.csv', 'WheelSize.csv', 
#    'ResourceCategory.csv', 'ResourceLanguage.csv', 'ResourceType.csv', 'Resource.csv', 'MaterialResource.csv', 'PartResource.csv',
#    'SuspensionType.csv', 'SuspensionSetUp.csv', 'SuspensionTypePart.csv', 'Maintenance.csv','AttributeCategory.csv','Attribute.csv',
#    'MaterialAttribute.csv','Maintenance.csv']
schema = 'Products'

for thisFile in dataFiles:
    thisTable = thisFile.split(".")[0]
    writeFile = open(os.path.join(data_folder, "ProductsData-" + thisTable + ".sql"), 'w')
    query = ''
    lineNumber = 0
    columns = []
    readFile = open(os.path.join(data_folder, thisFile), "r")
    lines = readFile.readlines()
    insertStatement = 'INSERT INTO Products.' + thisTable + '('
    for line in csv.reader(lines):
        if len(line) > 0:
            if lineNumber == 0:
                columnNumber = 0
                columns = line
                for column in line:
                    columnNumber += 1
                    if len(column.split(".")) > 1:
                        fromTable = column.split(".")[0].strip()
                        insertStatement += fromTable + 'Id'
                    else:
                        insertStatement += column.strip()
                    # add comma if not last column
                    if columnNumber < len(columns):
                        insertStatement += ','
                    else:
                        insertStatement += ', CreatedBy) VALUES('
            else:
                columnNumber = 0
                query += insertStatement
                for value in line:
                    value = value.replace("'", "''")
                    column = columns[columnNumber]
                    if len(column.split(".")) > 1:
                        fromTable = column.split(".")[0]
                        fromTableField = column.split(".")[1]
                        query += '(SELECT ' + fromTable + 'Id FROM ' + schema + '.' + fromTable + ' WHERE ' + fromTableField + ' = \'' + value.strip() + '\')'
                    else:
                        query += '\'' + value.strip() + '\''
                    
                    columnNumber += 1
                    # add comma if not last column
                    if columnNumber < len(line):
                        query += ', '
                    else:
                        query += ', (SELECT u.UserId FROM Users.UserRole ur INNER JOIN Users.[User] u on u.UserId = ur.UserId'
                        query += ' INNER JOIN Users.Role r ON r.RoleId = ur.RoleId WHERE u.GivenName = \'SAP\''
                        query += ' AND r.Name = \'System\'));\n'
            lineNumber += 1
    print(query)
    writeFile.write(query)
        
        # cursor = connection.cursor()
        # for data in reader:
        #     cursor.execute(query, data)
        # cursor.commit()