CREATE SCHEMA Products
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Products.Status (
    StatusId uniqueidentifier DEFAULT (newsequentialid()) UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.WheelSize (
    WheelSizeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL PRIMARY KEY,
    Name varchar(255) NOT NULL,
    Description varchar(255),
    WheelSize float(24) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.Manufacturer (
    ManufacturerId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    UserCreated bit DEFAULT ((0)) NOT NULL,
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.PlatformType (
    PlatformTypeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL PRIMARY KEY,
    Name varchar(255) NOT NULL,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.Platform (
    PlatformId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PlatformTypeId uniqueidentifier FOREIGN KEY REFERENCES Products.PlatformType(PlatformTypeId) NOT NULL,
    ManufacturerId uniqueidentifier FOREIGN KEY REFERENCES Products.Manufacturer(ManufacturerId) NOT NULL,
    Name varchar(255) NOT NULL,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PlatformTypeId, ManufacturerId, Name)
)
CREATE TABLE Products.Model (
    ModelId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PlatformId uniqueidentifier FOREIGN KEY REFERENCES Products.Platform(PlatformId) NOT NULL,
    ModelYear int NOT NULL,
    Name varchar(500) NOT NULL,
    Description varchar(250),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PlatformId, ModelYear, Name)
)
CREATE TABLE Products.Material (
    MaterialId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL PRIMARY KEY,
    MaterialNumber varchar(255),
    ModelId uniqueidentifier FOREIGN KEY REFERENCES Products.Model(ModelId) NOT NULL,
    Name varchar(1000) NOT NULL,
    Description varchar(1000),
    ActiveYear int NOT NULL DEFAULT(1900),
    Color varchar(255),
    FrameSize varchar(255),
    ImageURL varchar(255),
    UserCreated bit DEFAULT ((0)) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE UNIQUE INDEX UI_Material ON Products.Material (MaterialNumber) WHERE MaterialNumber IS NOT NULL
CREATE TABLE Products.MFDMaterial(
    MFDMaterialId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    MaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.Material(MaterialId) NOT NULL,
    SerialNumber varchar(255) NOT NULL,
    MFDDate datetime2(3),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (MaterialId, SerialNumber)
)
CREATE TABLE Products.UserMFDMaterial (
    UserMFDMaterialId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    UserId uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId) NOT NULL,
    MFDMaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.MFDMaterial(MFDMaterialId) NOT NULL,
    Name varchar(255),
    CrmId uniqueidentifier,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    StatusId uniqueidentifier FOREIGN KEY REFERENCES Products.Status(StatusId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (UserId, MFDMaterialId)
)
SET QUOTED_IDENTIFIER ON
GO
CREATE UNIQUE INDEX UI_UserMFDMaterial ON Products.UserMFDMaterial (CrmId) WHERE CrmId IS NOT NULL
CREATE TABLE Products.PartType (
    PartTypeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.AttributeCategory (
    AttributeCategoryId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.Attribute (
    AttributeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    AttributeCategoryId uniqueidentifier FOREIGN KEY REFERENCES Products.AttributeCategory(AttributeCategoryId),
    Name varchar(255) NOT NULL,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (AttributeCategoryId, Name)
)
CREATE TABLE Products.UserMFDMaterialAttribute (
    UserMFDMaterialAttributeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    UserMFDMaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.UserMFDMaterial(UserMFDMaterialId) NOT NULL,
    AttributeId uniqueidentifier FOREIGN KEY REFERENCES Products.Attribute(AttributeId) NOT NULL,
    DisplayOrder int,
    Value varchar(255) NOT NULL,
    Description varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (UserMFDMaterialId,AttributeId)
)
CREATE TABLE Products.Part (
    PartId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PartTypeId uniqueidentifier FOREIGN KEY REFERENCES Products.PartType(PartTypeId),
    Name varchar(500),
    Description varchar(255),
    ModelNumber varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PartTypeId, Name)
)
CREATE TABLE Products.PartAttribute (
    PartAttributeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PartId uniqueidentifier FOREIGN KEY REFERENCES Products.Part(PartId) NOT NULL,
    AttributeId uniqueidentifier FOREIGN KEY REFERENCES Products.Attribute(AttributeId) NOT NULL,
    DisplayOrder varchar(255),
    Value varchar(250) NOT NULL,
    Description varchar(500),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PartId, AttributeId, Value)
)
CREATE TABLE Products.MaterialAttribute (
    MaterialAttributeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    MaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.Material(MaterialId) NOT NULL,
    AttributeId uniqueidentifier FOREIGN KEY REFERENCES Products.Attribute(AttributeId) NOT NULL,
    DisplayOrder int NOT NULL DEFAULT(1),
    Value varchar(1000) NOT NULL,
    Description varchar(1000),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (MaterialAttributeId)
)
CREATE TABLE Products.MFDPart (
    MFDPartId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PartId uniqueidentifier FOREIGN KEY REFERENCES Products.Part(PartId) NOT NULL,
    SerialNumber varchar(255) NOT NULL,
    MFDDate datetime2(3),
    UserCreated bit DEFAULT ((0)) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PartId, SerialNumber)
)
CREATE TABLE Products.UserMFDPart (
    UserMFDPartId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    UserId uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId) NOT NULL,
    MFDPartId uniqueidentifier FOREIGN KEY REFERENCES Products.MFDPart(MFDPartId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (UserId, MFDPartId)
)
CREATE TABLE Products.UserMFDPartAttribute (
    UserMFDPartAttributeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    UserMFDPartId uniqueidentifier FOREIGN KEY REFERENCES Products.UserMFDPart(UserMFDPartId) NOT NULL,
    AttributeId uniqueidentifier FOREIGN KEY REFERENCES Products.Attribute(AttributeId) NOT NULL,
    DisplayOrder int,
    Value varchar(255) NOT NULL,
    Description varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (UserMFDPartAttributeId)
)
CREATE TABLE Products.MaterialBOM (
    MaterialBOMId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    MaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.Material(MaterialId) NOT NULL,
    PartId uniqueidentifier FOREIGN KEY REFERENCES Products.Part(PartId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (MaterialId, PartId)
)
CREATE TABLE Products.MFDMaterialBOM (
    MFDMaterialBOMId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    MFDMaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.MFDMaterial(MFDMaterialId) NOT NULL,
    MFDPartId uniqueidentifier FOREIGN KEY REFERENCES Products.MFDPart(MFDPartId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (MFDMaterialId, MFDPartId)
)
CREATE TABLE Products.UserMFDMaterialBOM (
    UserMFDMaterialBOMId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    UserMFDMaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.UserMFDMaterial(UserMFDMaterialId) NOT NULL,
    UserMFDPartId uniqueidentifier FOREIGN KEY REFERENCES Products.UserMFDPart(UserMFDPartId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (UserMFDMaterialId, UserMFDPartId)
)
CREATE TABLE Products.ResourceLanguage (
    ResourceLanguageId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.ResourceCategory (
    ResourceCategoryId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.ResourceType (
    ResourceTypeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.Resource (
    ResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    ResourceTypeId uniqueidentifier FOREIGN KEY REFERENCES Products.ResourceType(ResourceTypeId) NOT NULL,
    ResourceCategoryId uniqueidentifier FOREIGN KEY REFERENCES Products.ResourceCategory(ResourceCategoryId) NOT NULL,
    ResourceLanguageId uniqueidentifier FOREIGN KEY REFERENCES Products.ResourceLanguage(ResourceLanguageId) NOT NULL,
    Name varchar(255) NOT NULL,
    Description varchar(255),
    URL varchar(500) NOT NULL,
    Version varchar(255),
    ResourceDateUpdated datetime2(3),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (ResourceTypeId, ResourceCategoryId, ResourceLanguageId, Name, URL)
)
CREATE TABLE Products.ResourceRole (
    ResourceRoleId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId) NOT NULL,
    RoleId uniqueidentifier FOREIGN KEY REFERENCES Users.Role(RoleId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (ResourceId, RoleId)
)
CREATE TABLE Products.MaterialResource (
    MaterialResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    MaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.Material(MaterialId),
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (MaterialId, ResourceId)
)
CREATE TABLE Products.ModelResource (
    ModelResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    ModelId uniqueidentifier FOREIGN KEY REFERENCES Products.Model(ModelId),
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (ModelId, ResourceId)
)
CREATE TABLE Products.PlatformResource (
    PlatformResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PlatformId uniqueidentifier FOREIGN KEY REFERENCES Products.Platform(PlatformId),
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PlatformId, ResourceId)
)
CREATE TABLE Products.PlatformTypeResource (
    PlatformTypeResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PlatformTypeId uniqueidentifier FOREIGN KEY REFERENCES Products.PlatformType(PlatformTypeId) NOT NULL,
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PlatformTypeId, ResourceId)
)
CREATE TABLE Products.ManufacturerResource (
    ManufacturerResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    ManufacturerId uniqueidentifier FOREIGN KEY REFERENCES Products.Manufacturer(ManufacturerId) NOT NULL,
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (ManufacturerId, ResourceId)
)
CREATE TABLE Products.PartResource (
    PartResourceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PartId uniqueidentifier FOREIGN KEY REFERENCES Products.Part(PartId) NOT NULL,
    ResourceId uniqueidentifier FOREIGN KEY REFERENCES Products.Resource(ResourceId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PartId, ResourceId)
)
CREATE TABLE Products.Maintenance (
    MaintenanceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    RoleId uniqueidentifier FOREIGN KEY REFERENCES Users.[Role](RoleId) NOT NULL,
    Name varchar(255) NOT NULL,
    Description varchar(255),
    Recurring bit DEFAULT ((1)) NOT NULL,
    BeforeRide bit DEFAULT ((0)) NOT NULL,
    FrequencyRides int,
    FrequencyHours int,
    FrequencyMiles int,
    FrequencyYears float(24),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (RoleId, Name, Description)
)
CREATE TABLE Products.ModelMaintenance (
    ModelMaintenanceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    ModelId uniqueidentifier FOREIGN KEY REFERENCES Products.Model(ModelId) NOT NULL,
    MaintenanceId uniqueidentifier FOREIGN KEY REFERENCES Products.Maintenance(MaintenanceId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (ModelId, MaintenanceId)
)
CREATE TABLE Products.PartMaintenance (
    PartMaintenanceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    PartId uniqueidentifier FOREIGN KEY REFERENCES Products.Part(PartId) NOT NULL,
    MaintenanceId uniqueidentifier FOREIGN KEY REFERENCES Products.Maintenance(MaintenanceId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (PartId, MaintenanceId)
)
CREATE TABLE Products.UserMFDMaterialMaintenance (
    UserMFDMaterialMaintenanceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL PRIMARY KEY,
    UserMFDMaterialId uniqueidentifier FOREIGN KEY REFERENCES Products.UserMFDMaterial(UserMFDMaterialId) NOT NULL,
    ModelMaintenanceId uniqueidentifier FOREIGN KEY REFERENCES Products.ModelMaintenance(ModelMaintenanceId) NOT NULL,
    Notes varchar(max),
    ServiceDate datetime2(3) NOT NULL,
    DealerId int,
    Odometer decimal,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.UserMFDPartMaintenance (
    UserMFDPartMaintenanceId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL PRIMARY KEY,
    PartMaintenanceId uniqueidentifier FOREIGN KEY REFERENCES Products.PartMaintenance(PartMaintenanceId) NOT NULL,
    UserMFDPartId uniqueidentifier FOREIGN KEY REFERENCES Products.UserMFDPart(UserMFDPartId) NOT NULL,
    Notes varchar(max),
    ServiceDate datetime2(3) NOT NULL,
    DealerId int,
    Odometer decimal,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.SuspensionType (
    SuspensionTypeId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    Name varchar(255) NOT NULL PRIMARY KEY,
    Description varchar(255),
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL
)
CREATE TABLE Products.SuspensionSetUp (
    SuspensionSetUpId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    SuspensionTypeId uniqueidentifier FOREIGN KEY REFERENCES Products.SuspensionType(SuspensionTypeId) NOT NULL,
    RiderWeightInPounds int NOT NULL,
    PSI int NOT NULL,
    Clicks int,
    Coil int,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (SuspensionTypeId, RiderWeightInPounds, PSI, Clicks)
)
CREATE TABLE Products.SuspensionTypePart (
    SuspensionTypePartId uniqueidentifier DEFAULT (newsequentialid()) NOT NULL UNIQUE,
    SuspensionTypeId uniqueidentifier FOREIGN KEY REFERENCES Products.SuspensionType(SuspensionTypeId) NOT NULL,
    PartId uniqueidentifier FOREIGN KEY REFERENCES Products.Part(PartId) NOT NULL,
    DateCreated datetime2(3) DEFAULT GETDATE() NOT NULL,
    DateUpdated datetime2(3),
    CreatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    UpdatedBy uniqueidentifier FOREIGN KEY REFERENCES Users.[User](UserId),
    Active bit DEFAULT ((1)) NOT NULL,
    PRIMARY KEY (SuspensionTypeId, PartId)
)

GO


