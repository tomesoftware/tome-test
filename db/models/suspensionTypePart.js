/**
 * @overview SuespensionTypePart database ORM model
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const SuspensionTypePart = instance.define('SuspensionTypePart', {
        SuspensionTypePartId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            unique: true,
        },
        SuspensionTypeId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        PartId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });

    return SuspensionTypePart;
};
