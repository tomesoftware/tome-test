/**
 * @overview SuspensionSetUp database ORM model
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const SuspensionSetUp = instance.define('SuspensionSetUp', {
        SuspensionSetUpId: {
            type: Sequelize.UUIDV4,
            defaultValue: '(newsequentialid())',
            allowNull: false,
        },
        SuspensionTypeId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        RiderWeightInPounds: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        PSI: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        Clicks: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });

    return SuspensionSetUp;
};
