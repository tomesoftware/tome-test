/**
 * @overview Attribute database ORM model
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const Attribute = instance.define('Attribute', {
        AttributeId: {
            type: Sequelize.UUIDV4,
            autoIncrement: true,
            primaryKey: true,
        },
        AttributeCategoryId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return Attribute;
};
