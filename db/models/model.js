/**
 * @overview Model database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const Model = instance.define('Model', {
        ModelId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        PlatformId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        ModelYear: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return Model;
};
