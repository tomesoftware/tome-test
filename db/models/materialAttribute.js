/**
 * @overview MaterialAttribute database ORM model
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const MaterialAttribute = instance.define('MaterialAttribute', {
        MaterialAttributeId: {
            type: Sequelize.UUIDV4,
            autoIncrement: true,
            primaryKey: true,
        },
        AttributeId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        MaterialId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        Value: {
            type: Sequelize.STRING,
        },
        DisplayOrder: {
            type: Sequelize.INTEGER,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return MaterialAttribute;
};
