/**
 * @overview Resource database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const Resource = instance.define('Resource', {
        ResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            primaryKey: true,
        },
        ResourceTypeId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        ResourceCategoryId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        ResourceLanguageId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        URL: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        Version: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return Resource;
};
