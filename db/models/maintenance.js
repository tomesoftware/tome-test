/**
 * @overview Maintenance database ORM model
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const Maintenance = instance.define('Maintenance', {
        MaintenanceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        RoleId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        Recurring: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
        BeforeRide: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '0',
        },
        FrequencyHours: {
            type: Sequelize.INTEGER,
        },
        FrequencyMiles: {
            type: Sequelize.INTEGER,
        },
        FrequencyPerYear: {
            type: Sequelize.INTEGER,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return Maintenance;
};
