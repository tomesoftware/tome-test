/**
 * @overview UserMFDMaterialBOM database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const UserMFDMaterialBOM = instance.define('UserMFDMaterialBOM', {
        UserMFDMaterialBOMId: {
            type: Sequelize.UUIDV4,
            autoIncrement: true,
            primaryKey: true,
        },
        UserMFDMaterialId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UserMFDPartId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return UserMFDMaterialBOM;
};
