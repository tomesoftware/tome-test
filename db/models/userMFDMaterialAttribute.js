/**
 * @overview UserMFDPartAttribute database ORM model
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const UserMFDMaterialAttribute = instance.define('UserMFDMaterialAttribute', {
        UserMFDMaterialAttributeId: {
            type: Sequelize.UUIDV4,
            autoIncrement: true,
            primaryKey: true,
        },
        AttributeId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        UserMFDMaterialId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
        },
        Value: {
            type: Sequelize.STRING,
        },
        DisplayOrder: {
            type: Sequelize.INTEGER,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return UserMFDMaterialAttribute;
};
