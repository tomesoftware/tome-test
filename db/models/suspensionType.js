/**
 * @overview SuespensionType database ORM model
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

/*
    The main ID is "erroneously" marked as primary key because Sequelize does not support defining a many-to-many
    association and then using a non-primary key as the join key. I do not understand why.
*/

module.exports = (instance, Sequelize) => {
    const SuspensionType = instance.define('SuspensionType', {
        SuspensionTypeId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            unique: true,
            primaryKey: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });

    return SuspensionType;
};
