/**
 * @overview MaterialBOM database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const MaterialBOM = instance.define('MaterialBOM', {
        MaterialId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        PartId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return MaterialBOM;
};
