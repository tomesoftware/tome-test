/**
 * @overview Material database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const Material = instance.define('Material', {
        MaterialId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        MaterialNumber: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        ModelId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        Color: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        FrameSize: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        ImageURL: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UserCreated: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '0',
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return Material;
};
