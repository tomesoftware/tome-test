/**
 * @overview UserMFDPart database ORM model
 * @author Mike Swierenga
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const UserMFDPart = instance.define('UserMFDPart', {
        UserMFDPartId: {
            type: Sequelize.UUIDV4,
            autoIncrement: true,
            primaryKey: true,
        },
        MFDPartId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        UserId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });

    return UserMFDPart;
};
