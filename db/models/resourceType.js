/**
 * @overview ResourceType database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const ResourceType = instance.define('ResourceType', {
        ResourceTypeId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            primaryKey: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
            primaryKey: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return ResourceType;
};
