/**
 * @overview UserMFDMaterial database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const UserMFDMaterial = instance.define('UserMFDMaterial', {
        UserMFDMaterialId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            autoIncrement: true,
        },
        UserId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true,
        },
        MFDMaterialId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true,
        },
        CrmId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        StatusId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return UserMFDMaterial;
};
