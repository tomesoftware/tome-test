/**
 * @overview Material Resource database ORM models
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (sequelize, Sequelize) => {
    const MaterialResource = sequelize.define('MaterialResource', {
        MaterialResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            primaryKey: true,
        },
        MaterialId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        ResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return MaterialResource;
};
