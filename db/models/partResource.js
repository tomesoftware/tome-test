/**
 * @overview PartResource database ORM models
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (sequelize, Sequelize) => {
    const PartResource = sequelize.define('PartResource', {
        PartResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            primaryKey: true,
        },
        PartId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        ResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return PartResource;
};
