/**
 * @overview MFDMaterialBOM database ORM models
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const MFDMaterialBOM = instance.define('MFDMaterialBOM', {
        MFDMaterialId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        MFDPartId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });

    return MFDMaterialBOM;
};
