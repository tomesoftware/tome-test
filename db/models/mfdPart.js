/**
 * @overview MFDPart database ORM models
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const MFDPart = instance.define('MFDPart', {
        MFDPartId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        PartId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
        },
        SerialNumber: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        MFDDate: {
            type: Sequelize.DATE,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });

    return MFDPart;
};
