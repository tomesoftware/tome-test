/**
 * @overview UserMFDMaterialMaintenance database ORM model
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const UserMFDPartMaintenance = instance.define('UserMFDPartMaintenance', {
        UserMFDPartMaintenanceId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            autoIncrement: true,
        },
        UserMFDPartId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true,
        },
        PartMaintenanceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true,
        },
        Notes: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        ServiceDate: {
            type: Sequelize.DATE,
            allowNull: false,
        },
        DealerId: {
            type: Sequelize.INTEGER,
        },
        Odometer: {
            type: Sequelize.DECIMAL,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return UserMFDPartMaintenance;
};
