/**
 * @overview Manufacturer database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const Manufacturer = instance.define('Manufacturer', {
        ManufacturerId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Description: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        UserCreated: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '0',
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return Manufacturer;
};
