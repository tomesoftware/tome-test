/**
 * @overview ModelMaintenance database ORM models
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const ModelMaintenance = instance.define('ModelMaintenance', {
        ModelMaintenanceId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        ModelId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
        },
        MaintenanceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
    });

    return ModelMaintenance;
};
