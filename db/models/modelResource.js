/**
 * @overview ModelResource database ORM model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

module.exports = (instance, Sequelize) => {
    const ModelResource = instance.define('ModelResource', {
        ModelResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            defaultValue: '(newsequentialid())',
            primaryKey: true,
        },
        ModelId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        ResourceId: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        CreatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        UpdatedBy: {
            type: Sequelize.UUIDV4,
            allowNull: true,
        },
        Active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: '1',
        },
    });
    return ModelResource;
};
