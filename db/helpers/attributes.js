/**
 * @overview Attributes database functions
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

const db = require('../index');

/**
 * Find all parts (optionally of a given type)
 *
 * @param {object} optionals what to search for
 * @param {string} [optionals.serialNumber] serial number of the part
 * @param {string} [optionals.partType] name of part type
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getAttributes(
    optionals = {
        attributeName: null,
        attributeCategory: null,
    },
) {
    const searchParameters = {};
    searchParameters.attribute = {};
    searchParameters.attributeCategory = {};

    if (optionals.attributeName) {
        searchParameters.attribute.Name = optionals.attributeName;
    }

    if (optionals.attributeCategory) {
        searchParameters.attributeCategory.Name = optionals.attributeCategory;
    }

    return db.Attribute.findAll({
        required: true,
        where: searchParameters.attribute,
        include: [
            {
                model: db.AttributeCategory,
                required: true,
                where: searchParameters.attributeCategory,
            },
        ],
    });
}

function createUserMFDPartAttribute(
    userMFDPartId,
    attributeId,
    value,
    optionals = {
        userId: null,
        displayOrder: null,
    },
    sqlTransaction = null,
) {
    if (!userMFDPartId) {
        return Promise.reject(new Error('userMFDPartId is required to create a UserMFDPartAttribute'));
    }
    if (!attributeId) {
        return Promise.reject(new Error('attributeId is required to create a UserMFDPartAttribute'));
    }
    if (!value) {
        return Promise.reject(new Error('value is required to create a UserMFDPartAttribute'));
    }
    return db.UserMFDPartAttribute.findOrCreate({
        where: {
            UserMFDPartId: userMFDPartId,
            AttributeId: attributeId,
            Value: value,
        },
        defaults: {
            CreatedBy: optionals.userId,
            DisplayOrder: optionals.displayOrder,
        },
        transaction: sqlTransaction,
    });
}

const putUserMFDPartAttributes = async (newObject, userId, sqlTransaction = null) => {
    await db.UserMFDPartAttribute.update({
        Value: newObject.value,
        DisplayOrder: newObject.display_order,
        UpdatedBy: userId,
    }, {
        where: {
            UserMFDPartAttributeId: newObject.user_mfd_part_attribute_id,
        },
        transaction: sqlTransaction,
    });
};

function createUserMFDMaterialAttribute(
    userMFDMaterialId,
    attributeId,
    value,
    optionals = {
        userId: null,
        displayOrder: null,
    },
) {
    if (!userMFDMaterialId) {
        return Promise.reject(new Error('userMFDMaterialId is required to create a UserMFDMaterialAttribute'));
    }
    if (!attributeId) {
        return Promise.reject(new Error('attributeId is required to create a UserMFDMaterialAttribute'));
    }
    if (!value) {
        return Promise.reject(new Error('value is required to create a UserMFDMaterialAttribute'));
    }
    return db.UserMFDMaterialAttribute.findOrCreate({
        where: {
            UserMFDMaterialId: userMFDMaterialId,
            AttributeId: attributeId,
            Value: value,
        },
        defaults: {
            CreatedBy: optionals.userId,
            DisplayOrder: optionals.displayOrder,
        },
    });
}

const putUserMFDMaterialAttributes = async (newObject, userId, sqlTransaction = null) => {
    await db.UserMFDMaterialAttribute.update({
        Value: newObject.value,
        DisplayOrder: newObject.display_order,
        UpdatedBy: userId,
    }, {
        where: {
            UserMFDMaterialAttributeId: newObject.user_mfd_material_attribute_id,
        },
        transaction: sqlTransaction,
    });
};

module.exports = {
    getAttributes,
    createUserMFDPartAttribute,
    putUserMFDPartAttributes,
    createUserMFDMaterialAttribute,
    putUserMFDMaterialAttributes,
};
