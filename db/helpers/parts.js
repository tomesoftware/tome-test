/**
 * @overview Material database functions
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const _ = require('lodash');
const dbHelpers = require('./index.js');
const db = require('../index');

/**
 * Find all parts (optionally of a given type)
 *
 * @param {object} optionals what to search for
 * @param {string} [optionals.serialNumber] serial number of the part
 * @param {string} [optionals.partType] name of part type
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMFDParts(
    optionals = {
        serialNumber: null,
        partName: null,
        partType: null,
    },
) {
    const searchParameters = {};
    searchParameters.mfdPart = {};
    searchParameters.part = {};
    searchParameters.partType = {};

    if (optionals.serialNumber) {
        searchParameters.mfdPart.SerialNumber = optionals.serialNumber;
    }

    if (optionals.partName) {
        searchParameters.part.Name = optionals.partName;
    }

    if (optionals.partType) {
        searchParameters.partType.Name = optionals.partType;
    }

    return db.MFDPart.findAll({
        required: true,
        where: searchParameters.mfdPart,
        include: [
            {
                model: db.Part,
                required: true,
                where: searchParameters.part,
                include: [
                    {
                        model: db.PartType,
                        where: searchParameters.partType,
                    },
                ],
            },
        ],
    });
}

/**
 * Find all user parts (optionally of a given type, optionally for
 * a given user).
 *
 * @param {string} [partType] name of part type
 * @param {string} [userId] UUID of the user
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMFDPartsForUser(partType, userId, userMFDPart, includeAttributes) {
    const searchParameters = {};
    searchParameters.partType = {};
    searchParameters.userMFDPart = {};

    if (partType) {
        searchParameters.partType.Name = partType;
    }
    if (userId) {
        searchParameters.userMFDPart.UserId = userId;
    }
    if (userMFDPart) {
        searchParameters.userMFDPart.UserMFDPartId = userMFDPart;
    }

    const search = {
        where: searchParameters.userMFDPart,
        required: true,
        include: [
            {
                model: db.MFDPart,
                required: true,
                include: [
                    {
                        model: db.Part,
                        required: true,
                        include: [
                            {
                                model: db.PartType,
                                where: searchParameters.partType,
                            },
                        ],
                    },
                ],
            },
        ],
    };

    if (includeAttributes) {
        search.include.push({
            model: db.UserMFDPartAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }

    if (userMFDPart) {
        return db.UserMFDPart.findOne(search);
    } else {
        return db.UserMFDPart.findAll(search);
    }
}

/**
 * Set activation status for a user mfd part association
 * with an mfd material
 *
 * This sets or clears the active field for the association entry in
 * UserMFDMaterialBOM and UserMFDPart tables. If the UserMFDPart is
 * still associated with another active UserMFDMaterialBOM, then it remains
 * active.
 *
 * @param {boolean} active value to set for active
 * @param {string} userId UUID of the user
 * @param {string} UserMFDMaterialId UUID
 * @param {Transaction} [sqlTransaction] an optional SQL transaction to commit under
 * @return {Promise} the promise result of the sequelize query
*/
async function setActivationForUserMFDPartsForMFDMaterial(
    active,
    userMfdMaterialId,
    sqlTransaction = null,
) {
    const results = {};
    [results.user_mfd_material_boms] = await db.UserMFDMaterialBOM.update({
        Active: active,
    }, {
        where: {
            UserMFDMaterialId: userMfdMaterialId,
            Active: !active,
        },
        transaction: sqlTransaction,
    });
    // If that returns no results, it's fine, just means the bike has no parts

    // Get the full BOM for the UserMFDMaterial.
    // This query includes each of the UserMFDMaterialBOM entries
    // the UserMFDPart is included in, so we can deactivate only
    // the UserMFDParts that are no longer active on any
    // UserMFDMaterialBOM
    const boms = await db.UserMFDMaterialBOM.findAll({
        where: {
            UserMFDMaterialId: userMfdMaterialId,
        },
        transaction: sqlTransaction,
        include: [
            {
                model: db.UserMFDPart,
                required: true,
                include: [
                    {
                        model: db.UserMFDMaterialBOM,
                        required: true,
                    },
                ],
            },
        ],
    });

    // We have already set the Active flag for the UserMFDMaterialBOM, so just
    // set the active flag for the parts if it is asociated with an Active BOM
    if (boms.length) {
        const updatePromises = [];
        boms.forEach(bom => {
            // Array of BOMs that relate to a UserMFDPart that is related to the current BOM
            const relatedActiveBOMs = bom.dataValues.UserMFDPart.dataValues.UserMFDMaterialBOMs.map((b) => b.dataValues).filter((b) => b.Active);

            // If there are active BOMs, the part should be active.
            const shouldPartBeActive = relatedActiveBOMs.length > 0;

            // If part Active does not equal what it should
            if (shouldPartBeActive !== bom.dataValues.UserMFDPart.dataValues.Active) {
                // Make it so
                updatePromises.push(db.UserMFDPart.update({
                    Active: shouldPartBeActive,
                }, {
                    where: {
                        UserMFDPartId: bom.dataValues.UserMFDPart.dataValues.UserMFDPartId,
                    },
                    transaction: sqlTransaction,
                }));
            }
        });

        let updateCountList = await Promise.all(updatePromises);
        if (updateCountList.length) {
            updateCountList = _.flatten(updateCountList);
            results.user_mfd_parts = updateCountList.reduce((n, m) => n + m);
        } else {
            results.user_mfd_parts = 0;
        }
    } else {
        // No parts associated with material, so nothing to update, we're done here
        results.user_mfd_parts = 0;
    }

    return results;
}

function createUserMFDPart(
    userId,
    mfdPartId,
    sqlTransaction = null,
    optionals = {
        name: null,
    },
) {
    if (!userId) {
        return Promise.reject(new Error('UserId is required to create a UserMFDPart'));
    }
    if (!mfdPartId) {
        return Promise.reject(new Error('MFDPartId is required to create a UserMFDPart'));
    }

    return db.UserMFDPart.create({
        UserId: userId,
        MFDPartId: mfdPartId,
        Name: optionals.name,
        CreatedBy: userId,
    }, { transaction: sqlTransaction });
}

const createUserMFDPartAttributes = async (
    searchMethod = {
        userMFDMaterialBOM: null,
        userMFDPartId: null,
    },
    userMFDPartAttributes,
    userId,
    sqlTransaction = null,
) => {
    if (!searchMethod.userMFDMaterialBOM && !searchMethod.userMFDPartId) {
        return Promise.reject(new Error('Needs either userMFDMaterialBOM or userMFDPartId'));
    }
    const attributes = await Promise.all(userMFDPartAttributes.map(attribute => dbHelpers.attributes.getAttributes({
        attributeName: attribute.name,
        attributeCategory: attribute.category,
    })));
    const attributesToCreate = [];
    for (let index = 0; index < userMFDPartAttributes.length; index++) {
        const userMFDPartAttribute = userMFDPartAttributes[index];
        const optionals = {
            displayOrder: userMFDPartAttribute.display_order,
            userId: userId,
        };
        if (searchMethod.userMFDPartId && attributes[index].length > 0) {
            attributesToCreate.push(dbHelpers.attributes.createUserMFDPartAttribute(searchMethod.userMFDPartId, attributes[index][0].dataValues.AttributeId, userMFDPartAttribute.value, optionals, sqlTransaction));
        } else if (searchMethod.userMFDMaterialBOM && attributes[index].length > 0) {
            const userMFDPart = searchMethod.userMFDMaterialBOM.find(bom => bom.UserMFDPart.MFDPart.MFDPartId === userMFDPartAttribute.mfd_part_id);
            attributesToCreate.push(dbHelpers.attributes.createUserMFDPartAttribute(userMFDPart.dataValues.UserMFDPartId, attributes[index][0].dataValues.AttributeId, userMFDPartAttribute.value, optionals, sqlTransaction));
        }
    }
    return Promise.all(attributesToCreate);
};

async function createMFDPart(
    partName,
    serialNumber,
    sqlTransaction = null,
    optionals = {
        userId: null,
        userCreated: true,
    },
) {
    if (!partName) {
        return Promise.reject(new Error('partName is required to create a MFDPart'));
    }
    const userCreated = optionals.userCreated || true;


    const partWhere = {
        Name: partName,
    };

    const part = await db.Part.findOne({
        required: true,
        where: partWhere,
    });


    return db.MFDPart.create({
        PartId: part.dataValues.PartId,
        SerialNumber: serialNumber,
        CreatedBy: optionals.userId,
        UserCreated: userCreated,
    }, { transaction: sqlTransaction });
}

module.exports = {
    getMFDParts,
    getMFDPartsForUser,
    setActivationForUserMFDPartsForMFDMaterial,
    createUserMFDPart,
    createUserMFDPartAttributes,
    createMFDPart,
};
