/**
 * @overview Material database functions
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Boom = require('boom');

const db = require('../index');
const partsHelpers = require('./parts.js');
const attributesHelpers = require('./attributes.js');

/**
 * Helper method to build up the include statements for MFDMaterials
 * @param {boolean} [includeResources=false] Whether to get associated material and part resources
 * @param {boolean} [includeAttributes=false] Whether to get associated material and part attributes
 * @param {boolean} [dontIncludeUserCreated=false] Whether to get user created materials
 * @return {Object[]} Include array for MFDMaterial
 */
function buildMFDMaterialIncludes(includeResources = false, includeAttributes = false, dontIncludeUserCreated = false) {
    // Always include Material, Model
    // Platform, Manufacturer, & PlatformType
    const mfdMaterialIncludes = [
        {
            model: db.Material,
            include: [
                {
                    model: db.Model,
                    include: [
                        {
                            model: db.Platform,
                            include: [
                                { model: db.Manufacturer },
                                { model: db.PlatformType },
                            ],
                        },
                    ],
                },
            ],
        },
    ];

    // Allow caller to specify including Resource associations
    if (includeResources) {
        mfdMaterialIncludes[0].include[0].include.push({
            model: db.ModelResource,
            include: { all: true, nested: true },
        });
        mfdMaterialIncludes[0].include.push({
            model: db.MaterialResource,
            include: { all: true, nested: true },
        });
    }

    if (includeAttributes) {
        mfdMaterialIncludes[0].include.push({
            model: db.MaterialAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }

    if (dontIncludeUserCreated) {
        mfdMaterialIncludes[0].where = { UserCreated: false };
    }

    return mfdMaterialIncludes;
}

/**
 * Helper method to build up the include statements for Parts
 * @param {boolean} [includeResources=false] Whether to get associated material and part resources
 * @param {boolean} [includeAttributes=false] Whether to get associated material and part attributes
 * @return {Object[]} Include array for MFDPart
 */
function buildPartIncludes(includeResources = false, includeAttributes = false) {
    // Always include PartType association
    const partIncludes = [{ model: db.PartType }];

    // Allow caller to specify including Resource associations
    if (includeResources) {
        partIncludes.push({
            model: db.PartResource,
            include: { all: true, nested: true },
        });
    }

    if (includeAttributes) {
        partIncludes.push({
            model: db.PartAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }
    return partIncludes;
}

/**
 * Helper method to build up the include statements for UserMFDMaterialBOMs
 * @param {boolean} [includeResources=false] Whether to get associated material and part resources
 * @param {boolean} [includeAttributes=false] Whether to get associated material and part attributes
 * @param {boolean} [includeSetup=false] Whether to get associated material and part attributes
 * @return {Object[]} Include array for MFDPart
 */
function buildMFDPartIncludes(includeResources = false, includeAttributes = false, includeSetup = false) {
    // Always include PartType association
    const mfdPartIncludes = [
        {
            model: db.Part,
            include: [{ model: db.PartType }],
        },
    ];

    if (includeResources) {
        mfdPartIncludes[0].include.push({
            model: db.PartResource,
            include: { all: true, nested: true },
        });
    }

    if (includeAttributes) {
        mfdPartIncludes[0].include.push({
            model: db.PartAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }

    if (includeSetup) {
        mfdPartIncludes[0].include.push({
            model: db.SuspensionType,
            include: [
                {
                    model: db.SuspensionSetup,
                },
            ],
        });
    }

    return mfdPartIncludes;
}

/**
 * Helper method to build up the include statements for UserMFDMaterialBOMs
 * @param {boolean} [includeResources=false] Whether to get associated material and part resources
 * @param {boolean} [includeAttributes=false] Whether to get associated material and part attributes
 * @param {boolean} [includeSetup=false] Whether to get associated material and part setup
 * @return {Object[]} Include array for UserMFDMaterialBOM
 */
function buildUserMFDMaterialBOMIncludes(includeResources = false, includeAttributes = false, includeSetup = false) {
    const userMFDMaterialBOMIncludes = [
        {
            model: db.UserMFDPart,
            include: [
                {
                    model: db.MFDPart,
                    include: [
                        {
                            model: db.Part,
                            include: [
                                {
                                    model: db.PartType,
                                },
                            ],
                        },
                    ],
                },
            ],
        },
    ];

    if (includeResources) {
        userMFDMaterialBOMIncludes[0].include[0].include[0].include.push({
            model: db.PartResource,
            include: { all: true, nested: true },
        });
    }

    if (includeAttributes) {
        userMFDMaterialBOMIncludes[0].include.push({
            model: db.UserMFDPartAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }

    if (includeSetup) {
        userMFDMaterialBOMIncludes[0].include[0].include[0].include.push({
            model: db.SuspensionType,
            include: [
                {
                    model: db.SuspensionSetUp,
                },
            ],
        });
    }

    return userMFDMaterialBOMIncludes;
}

/**
 * Helper method to build up the include statements for MFDMaterialBOMs
 * @param {boolean} [includeResources=false] Whether to get associated material and part resources
 * @param {boolean} [includeAttributes=false] Whether to get associated material and part resources
 * @param {boolean} [includeSetup=false] Whether to get associated material and part setup
 * @return {Object[]} Include array for MFDMaterialBOM
 */
function buildMFDMaterialBOMIncludes(includeResources = false, includeAttributes = false, includeSetup = false) {
    const mfdMaterialBOMIncludes = [
        {
            model: db.MFDPart,
            include: [
                {
                    model: db.Part,
                    include: [
                        {
                            model: db.PartType,
                        },
                    ],
                },
            ],
        },
    ];

    if (includeResources) {
        mfdMaterialBOMIncludes[0].include[0].include.push({
            model: db.PartResource,
            include: { all: true, nested: true },
        });
    }

    if (includeAttributes) {
        mfdMaterialBOMIncludes[0].include[0].include.push({
            model: db.PartAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }

    if (includeSetup) {
        mfdMaterialBOMIncludes[0].include[0].include.push({
            model: db.SuspensionType,
            include: [
                {
                    model: db.SuspensionSetUp,
                },
            ],
        });
    }

    return mfdMaterialBOMIncludes;
}

function buildMaterialBOMIncludes(includeResources = false, includeSetup = false) {
    const materialBOMIncludes = [
        {
            model: db.Part,
            include: [
                {
                    model: db.PartType,
                },
            ],
        },
    ];

    if (includeResources) {
        materialBOMIncludes[0].include.push({
            model: db.PartResource,
            include: { all: true, nested: true },
        });
    }

    if (includeSetup) {
        materialBOMIncludes[0].include.push({
            model: db.SuspensionType,
            include: [
                {
                    model: db.SuspensionSetUp,
                },
            ],
        });
    }

    return materialBOMIncludes;
}

/**
 * Lookup a MFDMaterialBOM from a provided manufactured part serial number.
 * Also ensure that the part is of type 'Speed Sensor' by default.
 *
 * Basically the steps are:
 *  - Match the given serial number to a MFDPartId in MFDPart table
 *  - Match the MFDPart.PartId to a Part row
 *  - Match the Part.PartTypeId to a PartType row
 *  - Ensure the PartType.Name = 'Speed Sensor'
 *  - Match the MFDPart.MFDPartId to MFDMaterialId in MFDMaterialBOM table
 *  - Match the MFDMaterial.MFDMaterialId to MFDMaterial row(s)
 *
 * @param {string} partSerial Serial number from the part
 * @param {boolean} [includeResources=false] Whether to retrieve the associated part and material
 *  resources
 * @param {boolean} [includeAttributes=false] Whether to retrieve the associated material and part
 *  attributes
 * @param {string} [partType] Specify the name of a part type to filter by,
 *  defaults to 'Speed Sensor'
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMFDMaterialBOMFromPartSerialNumber(
    partSerial,
    includeResources = false,
    includeAttributes = false,
    partType = 'Speed Sensor',
) {
    return db.MFDMaterialBOM.findAll({
        required: true,
        include: [
            {
                model: db.MFDPart,
                where: { SerialNumber: partSerial },
                required: true,
                include: [
                    {
                        model: db.Part,
                        required: true,
                        include: [
                            {
                                model: db.PartType,
                                where: { Name: partType },
                            },
                        ],
                    },
                ],
            },
            {
                model: db.MFDMaterial,
                include: buildMFDMaterialIncludes(includeResources, includeAttributes),
            },
        ],
    });
}

/**
 * Lookup a MFDMaterial from a provided manufactured material (bike) serial number.
 *
 * @param {string} partSerial Serial number of the bike (MFDMaterial)
 * @param {boolean} [includeResources=false] Whether to retrieve the associated resources
 * @param {boolean} [includeAttributes=false] Whether to retrieve associated material and part attributes
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMFDMaterialFromBikeSerialNumber(
    bikeSerial,
    includeResources = false,
    includeAttributes = false,
    dontIncludeUserCreated = false,
) {
    return db.MFDMaterial.findAll({
        where: { SerialNumber: bikeSerial },
        include: buildMFDMaterialIncludes(includeResources, includeAttributes, dontIncludeUserCreated),
    });
}

/**
 * Lookup a MFDMaterial from a system ID
 *
 * @param {string} mfdMaterialId  UUID of the MFDMaterial
 * @param {boolean} [includeResources=false] Whether to retrieve the associated material and part
 *  resources
 * @param {boolean} [includeAttributes=false] Whether to retrieve thee associated material and part
 *  attributes
 */
function getMFDMaterialById(mfdMaterialId, includeResources = false, includeAttributes = false) {
    return db.MFDMaterial.findOne({
        where: { MFDMaterialId: mfdMaterialId },
        include: buildMFDMaterialIncludes(includeResources, includeAttributes),
    });
}

/**
 * Lookup a Part from a provided Material (bike) UUID.
 *
 * Basically the steps are:
 *  - Match the given Material UUID to any corresponding parts in the MaterialBOM table
 *
 * @param {string} materialId UUID of the Material
 * @param {boolean} [includeResources] Choose whether to retrieve the associated Resources,
 *  defaults to false
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getPartsForMaterial(materialId, includeResources = false, includeAttributes = false) {
    return db.MaterialBOM.findAll({
        where: { MaterialId: materialId },
        include: [
            {
                model: db.Part,
                include: buildPartIncludes(includeResources, includeAttributes),
            },
        ],
    });
}

/**
 * Lookup a MFDPart from a provided MFDMaterial (bike) UUID.
 *
 * Basically the steps are:
 *  - Match the given MFDMaterial UUID to any corresponding parts in the MFDMaterialBOM table
 *
 * @param {string} mfdMaterialId UUID of the MFDMaterial
 * @param {boolean} [includeResources] Choose whether to retrieve the associated Resources,
 *  defaults to false
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMFDPartsForMFDMaterial(mfdMaterialId, includeResources = false, includeAttributes = false) {
    return db.MFDMaterialBOM.findAll({
        where: { mfdMaterialId: mfdMaterialId },
        include: [
            {
                model: db.MFDPart,
                include: buildMFDPartIncludes(includeResources, includeAttributes),
            },
        ],
    });
}

/**
 * Create association between the given User & MFDPart.
 * Creates a new entry in the UserMFDPart table.
 *
 * @param {string} userId UUID of the User
 * @param {string} mfdPartId UUID of the MFDMaterial
 * @param {string} [createdBy] if present, UUID of a userId to be used
 *  for 'CreatedBy' fields
 * @return {Object} An object that contains data for the created record.
*/
async function associateUserWithMFDPart(
    userId,
    mfdPartId,
    createdBy = null,
    sqlTransaction = null,
) {
    try {
        // findOrCreate returns an array of two values:
        //  [0]: the found or created resource
        //  [1]: true if created, false if not created
        const [userMFDPart, newlyCreated] = await db.UserMFDPart.findOrCreate({
            where: {
                UserId: userId,
                MFDPartId: mfdPartId,
            },
            defaults: {
                CreatedBy: createdBy || userId,
                UpdatedBy: createdBy || userId,
            },
            transaction: sqlTransaction,
        });

        if (!newlyCreated) {
            userMFDPart.newly_created = newlyCreated;
            return userMFDPart;
        }

        // upsert doesn't return the newly created id, so we need to fetch it again
        const [userMFDPartWithId] = await db.UserMFDPart.findOrCreate({
            where: {
                UserId: userId,
                MFDPartId: mfdPartId,
            },
            defaults: {
                CreatedBy: createdBy || userId,
                UpdatedBy: createdBy || userId,
            },
            transaction: sqlTransaction,
        });

        userMFDPartWithId.newly_created = newlyCreated;
        return userMFDPartWithId;
    } catch (e) {
        throw Error(`Unable to associate user_id=${userId} with mfd_part_id=${mfdPartId}`);
    }
}

/**
 * Create association between the given User, MFDMaterial, & MFDPart.
 * Creates a new entry in the UserMFDMaterialBOM table.
 *
 * @param {string} userId UUID of the User
 * @param {string} mfdPartId UUID of the MFDPart
 * @param {string} mfdMaterialId UUID of the MFDMaterial
 * @param {string} [createdBy] if present, UUID of a userId to be used
 *  for 'CreatedBy' fields, otherwise userId is used
 * @return {Object} An object that contains data for the created record.
*/
async function associateUserWithMFDMaterialBOM(
    userMFDPartId,
    userMFDMaterialId,
    createdBy = null,
    sqlTransaction = null,
) {
    try {
        // findOrCreate returns an array of two values:
        //  [0]: the found or created resource
        //  [1]: true if created, false if not created
        const [userMFDMaterialBOM, newlyCreated] = await db.UserMFDMaterialBOM.findOrCreate({
            where: {
                UserMFDPartId: userMFDPartId,
                UserMFDMaterialId: userMFDMaterialId,
            },
            defaults: {
                CreatedBy: createdBy,
                UpdatedBy: createdBy,
            },
            transaction: sqlTransaction,
        });
        userMFDMaterialBOM.newly_created = newlyCreated;
        return userMFDMaterialBOM;
    } catch (e) {
        throw Error(`Unable to associate user_mfd_part_id=${userMFDPartId} with user_mfd_material_id=${userMFDMaterialId}`);
    }
}

/**
 * Create associations between the given User and MFDMaterial, including all of the MFDParts
 * that are known to be associated with the MFDMaterial.
 *
 * @param {string} userId UUID of the User
 * @param {string} mfdMaterialId UUID of the MFDMaterial
 * @param {string} [createdBy] if present, UUID of a userId to be used
 *  for 'CreatedBy' fields, otherwise userId is used
 * @return {Object} An object that contains data for all of the created records.
*/
async function associateUserWithMFDMaterial(
    userId,
    mfdMaterialId,
    cascadeAllParts = true,
    createdBy = null,
) {
    const results = {};
    try {
        const statusIdQuery = await db.Status.find({
            where: {
                Name: 'Active',
            },
            attributes: ['StatusId'],
        });
        if (!statusIdQuery) {
            throw new Boom('Could not find Active status in db');
        }
        const activeStatusId = statusIdQuery.dataValues.StatusId;
        // findOrCreate returns an array of two values:
        // [0]: the found or created resource
        // [1]: true if created, false if not created
        const [userMFDMaterial, newlyCreated] = await db.UserMFDMaterial.findOrCreate({
            where: {
                MFDMaterialId: mfdMaterialId,
                UserId: userId,
            },
            defaults: {
                CreatedBy: createdBy || userId,
                UpdatedBy: createdBy || userId,
                StatusId: activeStatusId,
            },
            // This shouldn't be here, findOrCreate doesn't return?
            // include: [{ model: db.MFDMaterial, include: buildMFDMaterialIncludes() }],
        });

        if (!newlyCreated) {
            userMFDMaterial.newly_created = newlyCreated;
            results.user_mfd_material = userMFDMaterial;
            return results;
        }

        // findOrCreate doesn't return, so we need to fetch it again
        const [userMFDMaterialWithMaterial] = await db.UserMFDMaterial.findOrCreate({
            where: {
                MFDMaterialId: mfdMaterialId,
                UserId: userId,
            },
            defaults: {
                CreatedBy: createdBy || userId,
                UpdatedBy: createdBy || userId,
            },
            include: [{ model: db.MFDMaterial, include: buildMFDMaterialIncludes() }],
        });
        userMFDMaterialWithMaterial.newly_created = newlyCreated;
        results.user_mfd_material = userMFDMaterialWithMaterial;
    } catch (e) {
        console.log(e);
        throw Error(`Unable to associate user_id=${userId} with mfd_material_id=${mfdMaterialId}`);
    }

    if (cascadeAllParts && results.user_mfd_material.newly_created) {
        const parts = await getPartsForMaterial(results.user_mfd_material.MFDMaterial.Material.dataValues.MaterialId);
        const mfdParts = await getMFDPartsForMFDMaterial(mfdMaterialId);
        const goodParts = parts.filter(partBOM => {
            const isTheCorrectType = ['Fork', 'Rear Shock'].includes(partBOM.Part.PartType.dataValues.Name);
            const mfdPart = mfdParts.find(mPart => mPart.MFDPart.Part.dataValues.PartId === partBOM.Part.dataValues.PartId);
            return isTheCorrectType && !mfdPart;
        });
        const asyncCreateMFDPart = (part) => partsHelpers.createMFDPart(part.Part.dataValues.Name);
        const createdMFDParts = await Promise.all(goodParts.map(asyncCreateMFDPart));
        // For each associated MFDPart, insert association entry into both
        // UserMFDPart & UserMFDMaterialBOM
        const asyncAssociateMFDPart = (part) => associateUserWithMFDPart(userId, part.MFDPartId);
        results.user_mfd_parts = await Promise.all(mfdParts.concat(createdMFDParts).map(asyncAssociateMFDPart));
        const userMFDMaterialId = results.user_mfd_material.dataValues.UserMFDMaterialId;
        const asyncAssociateMFDMaterialBOM = (userMFDPart) => associateUserWithMFDMaterialBOM(userMFDPart.dataValues.UserMFDPartId, userMFDMaterialId, userId);
        results.user_mfd_material_boms = await Promise.all(results.user_mfd_parts.map(asyncAssociateMFDMaterialBOM));
    }
    return results;
}

/**
 * Find all associated MFDParts for a MFDMaterial that is associated with a User.
 *
 * @param {string} userMFDMaterialId UUID of the MFDMaterial
 * @param {boolean} [includeResources] Whether to retrieve the part's resources
 * @param {boolean} [includeAttributes] Whether to retrieve the part's attributes
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getUserMFDMaterialBOM(
    userMFDMaterialId,
    includeResources = false,
    includeAttributes = false,
    includeSetup = false,
) {
    return db.UserMFDMaterialBOM.findAll({
        where: {
            UserMFDMaterialId: userMFDMaterialId,
            Active: 1,
        },
        include: buildUserMFDMaterialBOMIncludes(includeResources, includeAttributes, includeSetup),
    });
}

/**
 * Find all associated MFDParts for a MFDMaterial.
 *
 * @param {string} userId UUID of the User
 * @param {string} mfdMaterialId UUID of the MFDMaterial
 * @param {boolean} [includeResources] Choose whether to retrieve the associated Resources,
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMFDMaterialBOM(
    mfdMaterialId,
    includeResources = false,
    includeSetup = false,
) {
    return db.MFDMaterialBOM.findAll({
        where: {
            MFDMaterialID: mfdMaterialId,
            Active: 1,
        },
        include: buildMFDMaterialBOMIncludes(includeResources, includeSetup),
    });
}

/**
 * Find all associated Parts for a Material
 *
 * @param {string} materialId UUID of the MFDMaterial
 * @param {boolean} [includeResources] Choose whether to retrieve the associated Resources,
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getMaterialBOM(
    materialId,
    includeResources = false,
    includeSetup = false,
) {
    return db.MaterialBOM.findAll({
        where: {
            MaterialId: materialId,
            Active: 1,
        },
        include: buildMaterialBOMIncludes(includeResources, includeSetup),
    });
}

/**
 * Find an association between a User and MFDMaterial.
 *
 * @param {string} userId UUID of the User
 * @param {string} mfdMaterialId UUID of the MFDMaterial
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getUserMFDMaterial(
    userId,
    searchCriteria = {
        mfdMaterialId: null,
        userMFDMaterialId: null,
    },
    includeAttributes = false,
) {
    if (!searchCriteria.mfdMaterialId && !searchCriteria.userMFDMaterialId) {
        throw Error('Must have either mfdMaterialId or userMFDMaterialId');
    }

    const search = {
        where: {
            UserId: userId,
            Active: 1,
        },
        include: [
            {
                model: db.MFDMaterial,
                include: buildMFDMaterialIncludes(false, includeAttributes),
            },
            {
                model: db.Status,
            },
        ],
    };

    if (searchCriteria.mfdMaterialId) {
        search.where.MFDMaterialId = searchCriteria.mfdMaterialId;
    }

    if (searchCriteria.userMFDMaterialId) {
        search.where.UserMFDMaterialId = searchCriteria.userMFDMaterialId;
    }

    if (includeAttributes) {
        search.include.push({
            model: db.UserMFDMaterialAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }

    return db.UserMFDMaterial.findOne(search);
}

/**
 * Find all UserMFDMaterials associated with a single user
 *
 * @param {string} userId UUID of the User
 * @param {boolean} [includeResources=false] Whether to retrieve the associated resources
 * @param {boolean} [includeAttributes=false] Whether to retrieve the associated attributes
 * @return {Promise} the Bluebird promise result of the sequelize query
*/
function getAllUserMFDMaterials(userId, includeResources = false, includeAttributes = false) {
    const searchParams = {
        where: {
            UserId: userId,
            Active: 1,
        },
        include: [
            {
                required: true,
                model: db.MFDMaterial,
                include: buildMFDMaterialIncludes(includeResources, includeAttributes),
            },
            {
                model: db.Status,
            },
        ],
    };
    if (includeAttributes) {
        searchParams.include.push({
            model: db.UserMFDMaterialAttribute,
            include: [
                {
                    model: db.Attribute,
                    include: [
                        {
                            model: db.AttributeCategory,
                        },
                    ],
                },
            ],
        });
    }
    return db.UserMFDMaterial.findAll(searchParams);
}

/**
 * Set activation status for a user mfd material association.
 *
 * This sets or clears the active field for the association entry in
 * UserMFDMaterial and cascades for all associated parts in
 * UserMFDMaterialBOM and UserMFDPart tables.
 *
 * @param {boolean} active value to set for active
 * @param {string} userMFDMaterialId UUID of the mfd material
 * @param {Transaction} [sqlTransaction] an optional SQL transaction to commit under
 * @return {Promise} the promise result of the sequelize query
*/
async function setActivationForUserMFDMaterial(
    active,
    userMfdMaterialId,
    sqlTransaction = null,
) {
    // set active on an entry in UserMFDMaterial
    const results = {};
    [results.user_mfd_materials] = await db.UserMFDMaterial.update({
        Active: active,
    }, {
        where: {
            UserMFDMaterialId: userMfdMaterialId,
            Active: !active,
        },
        transaction: sqlTransaction,
    });

    // If none were updated, then we should check if it exists and return
    // 404 if it doesn't
    if (!results.user_mfd_materials) {
        const exists = await db.UserMFDMaterial.count({
            where: {
                UserMFDMaterialId: userMfdMaterialId,
            },
            transaction: sqlTransaction,
        });
        if (!exists) {
            throw Boom.notFound(`No UserMFDMaterial found for user where user_mfd_material_id = ${userMfdMaterialId}`);
        }
        // If this is the case, it exists, but active was already set to the correct value
        return results;
    }

    // Now make this happen for UserMFDMaterialBOM and UserMFDPart
    const partsResults = await partsHelpers.setActivationForUserMFDPartsForMFDMaterial(
        active, userMfdMaterialId, sqlTransaction,
    );

    Object.assign(results, partsResults);
    return results;
}

function createMaterial(
    materialName,
    sqlTransaction,
    optionals = {
        modelId: null,
        color: null,
        frameSize: null,
        description: null,
        materialNumber: null,
        imageUrl: null,
        createdBy: null,
        userCreated: true,
    },
) {
    if (!materialName) {
        return Promise.reject(new Error('Material.Name is required to create Material'));
    }
    const userCreated = optionals.userCreated || true;

    return db.Material.create({
        ModelId: optionals.modelId,
        Name: materialName,
        MaterialNumber: optionals.materialNumber,
        Description: optionals.description,
        Color: optionals.color,
        FrameSize: optionals.frameSize,
        ImageURL: optionals.imageUrl,
        CreatedBy: optionals.createdBy,
        UserCreated: userCreated,
    }, { transaction: sqlTransaction });
}

function createMFDMaterial(
    sqlTransaction = null,
    optionals = {
        createdBy: null,
        serialNumber: null,
        materialId: null,
        mfdDate: null,
    },
) {
    return db.MFDMaterial.create({
        MaterialId: optionals.materialId,
        SerialNumber: optionals.serialNumber,
        MFDDate: optionals.mfdDate,
        CreatedBy: optionals.createdBy,
    }, { transaction: sqlTransaction });
}

function createUserMFDMaterial(
    userId,
    mfdMaterialId,
    statusId,
    sqlTransaction = null,
    optionals = {
        name: null,
        actualCircumference: null,
    },
) {
    if (!userId) {
        return Promise.reject(new Error('UserId is required to create a UserMFDMaterial'));
    }
    if (!mfdMaterialId) {
        return Promise.reject(new Error('MFDMaterialId is required to create a UserMFDMaterial'));
    }

    return db.UserMFDMaterial.create({
        UserId: userId,
        MFDMaterialId: mfdMaterialId,
        Name: optionals.name,
        StatusId: statusId,
        CreatedBy: userId,
    }, { transaction: sqlTransaction });
}

const createUserMFDMaterialAttributes = async (
    userMFDMaterialId,
    userMFDMaterialAttributes,
    userId,
) => {
    const attributes = await Promise.all(userMFDMaterialAttributes.map(attribute => attributesHelpers.getAttributes({
        attributeName: attribute.name,
        attributeCategory: attribute.category,
    })));
    const attributesToCreate = [];
    for (let index = 0; index < userMFDMaterialAttributes.length; index++) {
        const userMFDMaterialAttribute = userMFDMaterialAttributes[index];
        const optionals = {
            displayOrder: userMFDMaterialAttribute.display_order,
            userId: userId,
        };
        attributesToCreate.push(attributesHelpers.createUserMFDMaterialAttribute(userMFDMaterialId, attributes[index][0].dataValues.AttributeId, userMFDMaterialAttribute.value, optionals));
    }
    return Promise.all(attributesToCreate);
};

function getStatuses() {
    return db.Status.findAll({
        attributes: ['StatusId', 'Name', 'Description'],
    });
}

function getWheelSizes() {
    return db.WheelSize.findAll({
        attributes: ['WheelSizeId', 'Name', 'Description', 'WheelSize'],
    });
}
const updateUserMFDMaterial = async (
    userMFDMaterialId,
    userId,
    name,
    statusId,
) => {
    const fieldsToUpdate = {};
    if (name) {
        fieldsToUpdate.Name = name;
    }
    if (statusId) {
        fieldsToUpdate.StatusId = statusId;
    }
    const where = {
        UserMFDMaterialId: userMFDMaterialId,
        UserId: userId,
        Active: 1,
    };
    return db.UserMFDMaterial.update(
        fieldsToUpdate,
        {
            where: where,
        },
    ).then(() => db.UserMFDMaterial.findOne({
        where: where,
        include: [
            {
                model: db.MFDMaterial,
                include: buildMFDMaterialIncludes(false, false),
            },
            {
                model: db.Status,
            },
        ],
    }));
};

module.exports = {
    getMFDMaterialBOMFromPartSerialNumber,
    getMFDMaterialFromBikeSerialNumber,
    getMFDMaterialById,
    getMFDPartsForMFDMaterial,
    getMaterialBOM,
    getMFDMaterialBOM,
    associateUserWithMFDMaterial,
    associateUserWithMFDPart,
    associateUserWithMFDMaterialBOM,
    getUserMFDMaterialBOM,
    getUserMFDMaterial,
    getAllUserMFDMaterials,
    setActivationForUserMFDMaterial,
    createMaterial,
    createMFDMaterial,
    createUserMFDMaterial,
    createUserMFDMaterialAttributes,
    getWheelSizes,
    getStatuses,
    updateUserMFDMaterial,
};
