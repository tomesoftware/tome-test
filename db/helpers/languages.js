/**
 * @overview Language database functions
 * @author Matt Rathbun
 * @copyright Tome Software 2019
 */

'use strict';

const db = require('../index');

/**
 * Get a specific language record
 */
function getLanguage(id) {
    return db.ResourceLanguage.findOne({
        where: { ResourceLanguageId: id },
    });
}

/**
 * Get all resource language records
 *
 * @return {Promise} Sequelize query result
 */
function getLanguages() {
    return db.ResourceLanguage.findAll();
}

module.exports = {
    getLanguage,
    getLanguages,
};
