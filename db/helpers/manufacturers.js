/**
 * @overview Manufacturer database functions
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const db = require('../index');

const DEFAULT_MANUFACTURER_NAME = '<None>';

/**
 *
 */
function findOrCreateManufacturer(
    sqlTransaction = null,
    optionals = {
        name: DEFAULT_MANUFACTURER_NAME,
        createdBy: null,
    },
) {
    return db.Manufacturer.findOrCreate({
        where: {
            Name: optionals.name || DEFAULT_MANUFACTURER_NAME,
        },
        defaults: {
            Name: optionals.name || DEFAULT_MANUFACTURER_NAME,
            CreatedBy: optionals.createdBy,
            UserCreated: true,
        },
        transaction: sqlTransaction,
    });
}

function getManufacturers() {
    return db.Manufacturer.findAll({
        where: {
            UserCreated: false,
        },
        include: [
            {
                model: db.Platform,
                include: [
                    {
                        model: db.Model,
                        include: [
                            {
                                model: db.Material,
                            },
                        ],

                    },
                ],
            },
        ],
    });
}

module.exports = {
    findOrCreateManufacturer,
    getManufacturers,
};
