/**
 * @overview Maintenances database functions
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

const db = require('../index');

/**
 * Get a list of services that can be performed and services that have been performed for a given
 * user and UserMFDMaterialId.
 */
function getMaintenances(
    userId,
    userMFDMaterialId,
    optionals = {
        includePerformed: false,
    },
) {
    const modelMaintenanceIncludes = [{
        model: db.Maintenance,
    }];
    const partMaintenanceIncludes = [{
        model: db.Maintenance,
    }];

    if (optionals.includePerformed) {
        modelMaintenanceIncludes.push({
            model: db.UserMFDMaterialMaintenance,
        });
        partMaintenanceIncludes.push({
            model: db.UserMFDPartMaintenance,
        });
    }

    return db.UserMFDMaterial.findAll({
        required: true,
        where: {
            UserId: userId,
            UserMFDMaterialId: userMFDMaterialId,
        },
        include: [
            {
                model: db.MFDMaterial,
                include: [
                    {
                        model: db.Material,
                        include: [
                            {
                                model: db.Model,
                                include: [
                                    {
                                        model: db.ModelMaintenance,
                                        include: modelMaintenanceIncludes,
                                    },
                                ],
                            },
                        ],
                    },
                ],
            }, {
                model: db.UserMFDMaterialBOM,
                include: [
                    {
                        model: db.UserMFDPart,
                        include: [
                            {
                                model: db.MFDPart,
                                include: [
                                    {
                                        model: db.Part,
                                        include: [
                                            {
                                                model: db.PartMaintenance,
                                                include: partMaintenanceIncludes,
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    });
}

/**
 * Create a new UserMFDMaterialMaintenance.
 */
function createUserMFDMaterialMaintenance(
    userMFDMaterialId,
    modelMaintenanceId,
    optionals = {
        serviceDate: new Date(),
        notes: null,
        dealerId: null,
        odometer: null,
    },
) {
    if (!userMFDMaterialId) {
        return Promise.reject(new Error('UserMFDMaterialId is required to create a UserMFDMaterialMaintenance'));
    }
    if (!modelMaintenanceId) {
        return Promise.reject(new Error('MaterialMaintenanceId is required to create a UserMFDMaterialMaintenance'));
    }

    return db.UserMFDMaterialMaintenance.create({
        UserMFDMaterialId: userMFDMaterialId,
        ModelMaintenanceId: modelMaintenanceId,
        Notes: optionals.notes,
        Odometer: optionals.odometer,
        ServiceDate: optionals.serviceDate || new Date(),
        DealerId: optionals.dealerId,
    });
}

/**
 * Create a new UserMFDPartMaintenance.
 */
function createUserMFDPartMaintenance(
    userMFDPartId,
    partMaintenanceId,
    optionals = {
        serviceDate: new Date(),
        notes: null,
        dealerId: null,
        odometer: null,
    },
) {
    if (!userMFDPartId) {
        return Promise.reject(new Error('UserMFDPartId is required to create a UserMFDPartMaintenance'));
    }
    if (!partMaintenanceId) {
        return Promise.reject(new Error('PartMaintenanceId is required to create a UserMFDPartMaintenance'));
    }

    return db.UserMFDPartMaintenance.create({
        UserMFDPartId: userMFDPartId,
        PartMaintenanceId: partMaintenanceId,
        Notes: optionals.notes,
        Odometer: optionals.odometer,
        ServiceDate: optionals.serviceDate || new Date(),
        DealerId: optionals.dealerId,
    });
}

module.exports = {
    getMaintenances,
    createUserMFDMaterialMaintenance,
    createUserMFDPartMaintenance,
};
