/**
 * @overview Model database functions
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const db = require('../index');

const DEFAULT_MODEL_NAME = '<None>';
const DEFAULT_MODEL_YEAR = 1900;

/**
 *
 */
function createModel(
    sqlTransaction,
    optionals = {
        name: DEFAULT_MODEL_NAME,
        platformId: null,
        createdBy: null,
        modelYear: null,
    },
) {
    return db.Model.create({
        PlatformId: optionals.platformId,
        Name: optionals.name || DEFAULT_MODEL_NAME,
        CreatedBy: optionals.createdBy,
        ModelYear: optionals.modelYear || DEFAULT_MODEL_YEAR,
    }, { transaction: sqlTransaction });
}

module.exports = {
    createModel,
};
