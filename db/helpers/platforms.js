/**
 * @overview Platform database functions
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const db = require('../index');

const DEFAULT_PLATFORM_NAME = '<None>';
const DEFAULT_PLATFORM_TYPE_NAME = 'Custom';

/**
 *
 */
function createPlatform(
    sqlTransaction,
    optionals = {
        name: DEFAULT_PLATFORM_NAME,
        manufacturerId: null,
        createdBy: null,
        platformTypeId: null,
    },
) {
    return db.Platform.create({
        Name: optionals.name || DEFAULT_PLATFORM_NAME,
        ManufacturerId: optionals.manufacturerId,
        CreatedBy: optionals.createdBy,
        PlatformTypeId: optionals.platformTypeId,
    }, { transaction: sqlTransaction });
}

/**
 *
 */
function createPlatformType(
    sqlTransaction,
    optionals = {
        name: DEFAULT_PLATFORM_TYPE_NAME,
        createdBy: null,
    },
) {
    return db.PlatformType.create({
        Name: optionals.name || DEFAULT_PLATFORM_TYPE_NAME,
        CreatedBy: optionals.createdBy,
    }, { transaction: sqlTransaction });
}

module.exports = {
    createPlatform,
    createPlatformType,
};
