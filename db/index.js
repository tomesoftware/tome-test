/**
 * @overview Database setup
 * @author Ben Willshire
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const dbHost = process.env.DB_HOST;
const dbDatabase = process.env.DB_DATABASE;
const dbUser = process.env.DB_USER;
const dbPassword = process.env.DB_PASSWORD;

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(__filename);
const db = {};

// Creating a new database instance from the connection details:
const sequelize = new Sequelize(dbDatabase, dbUser, dbPassword, {
    host: dbHost,
    dialect: 'mssql',
    operatorsAliases: false,

    logging: (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'development_no_logging'),

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },

    dialectOptions: {
        encrypt: true, // Use this if you're on Windows Azure
    },

    // Default options for a Model definition
    define: {
        timestamps: true,
        createdAt: 'DateCreated',
        updatedAt: 'DateUpdated',
        freezeTableName: true,
        schema: 'Products',
    },
});

// Test the connection immediately
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

// Load each sequelize model definition
fs
    .readdirSync(`${__dirname}/models`)
    .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
    .forEach(file => {
        const model = sequelize.import(path.join(`${__dirname}/models`, file));
        db[model.name] = model;
    });

// Wire up the relationships between tables
db.Manufacturer.hasMany(db.Platform, { foreignKey: 'ManufacturerId' });
db.Platform.belongsTo(db.Manufacturer, { foreignKey: 'ManufacturerId' });

db.Platform.belongsTo(db.PlatformType, { foreignKey: 'PlatformTypeId' });

db.Model.belongsTo(db.Platform, { foreignKey: 'PlatformId' });
db.Platform.hasMany(db.Model, { foreignKey: 'PlatformId' });

db.Material.belongsTo(db.Model, { foreignKey: 'ModelId' });
db.Model.hasMany(db.Material, { foreignKey: 'ModelId' });

db.MFDMaterial.belongsTo(db.Material, { foreignKey: 'MaterialId' });
db.MFDMaterialBOM.belongsTo(db.MFDMaterial, { foreignKey: 'MFDMaterialId' });
db.MFDMaterialBOM.belongsTo(db.MFDPart, { foreignKey: 'MFDPartId' });
db.MFDPart.belongsTo(db.Part, { foreignKey: 'PartId' });
db.Part.belongsTo(db.PartType, { foreignKey: 'PartTypeId' });
db.Resource.belongsTo(db.ResourceType, { foreignKey: 'ResourceTypeId' });
db.Resource.belongsTo(db.ResourceLanguage, { foreignKey: 'ResourceLanguageId' });
db.Resource.belongsTo(db.ResourceCategory, { foreignKey: 'ResourceCategoryId' });
db.ModelResource.belongsTo(db.Resource, { foreignKey: 'ResourceId' });
db.PartResource.belongsTo(db.Resource, { foreignKey: 'ResourceId' });
db.MaterialResource.belongsTo(db.Resource, { foreignKey: 'ResourceId' });
db.Material.hasMany(db.MaterialResource, { foreignKey: 'MaterialId' });
db.Part.hasMany(db.PartResource, { foreignKey: 'PartId' });
db.Model.hasMany(db.ModelResource, { foreignKey: 'ModelId' });
db.UserMFDPart.belongsTo(db.MFDPart, { foreignKey: 'MFDPartId' });
db.UserMFDMaterial.belongsTo(db.MFDMaterial, { foreignKey: 'MFDMaterialId' });
db.UserMFDMaterial.belongsTo(db.Status, { foreignKey: 'StatusId' });

db.UserMFDMaterialBOM.belongsTo(db.UserMFDPart, { foreignKey: 'UserMFDPartId' });
db.UserMFDPart.hasMany(db.UserMFDMaterialBOM, { foreignKey: 'UserMFDPartId' });
db.UserMFDMaterialBOM.belongsTo(db.UserMFDMaterial, { foreignKey: 'UserMFDMaterialId' });
db.UserMFDMaterial.hasMany(db.UserMFDMaterialBOM, { foreignKey: 'UserMFDMaterialId' });
db.Part.hasMany(db.MaterialBOM, { foreignKey: 'PartId' });
db.MaterialBOM.belongsTo(db.Part, { foreignKey: 'PartId' });
db.MaterialBOM.belongsTo(db.Material, { foreignKey: 'MaterialId' });
db.Attribute.belongsTo(db.AttributeCategory, { foreignKey: 'AttributeCategoryId' });
db.UserMFDPartAttribute.belongsTo(db.Attribute, { foreignKey: 'AttributeId' });
db.UserMFDPart.hasMany(db.UserMFDPartAttribute, { foreignKey: 'UserMFDPartId' });
db.UserMFDMaterialAttribute.belongsTo(db.Attribute, { foreignKey: 'AttributeId' });
db.UserMFDMaterial.hasMany(db.UserMFDMaterialAttribute, { foreignKey: 'UserMFDMaterialId' });
db.MaterialAttribute.belongsTo(db.Attribute, { foreignKey: 'AttributeId' });
db.Material.hasMany(db.MaterialAttribute, { foreignKey: 'MaterialId' });
db.PartAttribute.belongsTo(db.Attribute, { foreignKey: 'AttributeId' });
db.Part.hasMany(db.PartAttribute, { foreignKey: 'PartId' });

// Maintenances for models
db.ModelMaintenance.belongsTo(db.Maintenance, { foreignKey: 'MaintenanceId' });
db.Model.hasMany(db.ModelMaintenance, { foreignKey: 'ModelId' });

// Maintenances for parts
db.Part.hasMany(db.PartMaintenance, { foreignKey: 'PartId' });
db.PartMaintenance.belongsTo(db.Maintenance, { foreignKey: 'MaintenanceId' });

// User maintenances for model and part maintenances
db.ModelMaintenance.hasMany(db.UserMFDMaterialMaintenance, { foreignKey: 'ModelMaintenanceId' });
db.PartMaintenance.hasMany(db.UserMFDPartMaintenance, { foreignKey: 'PartMaintenanceId' });

// Suspension relationships
db.SuspensionType.belongsToMany(db.Part, { through: db.SuspensionTypePart, foreignKey: 'SuspensionTypeId', otherKey: 'PartId' });
db.Part.belongsToMany(db.SuspensionType, { through: db.SuspensionTypePart, foreignKey: 'PartId', otherKey: 'SuspensionTypeId' });
db.SuspensionType.hasMany(db.SuspensionSetUp, { foreignKey: 'SuspensionTypeId' });

// Expose access to the sequelize instance
db.sequelize = sequelize;

// Exporting the database object for shared use:
module.exports = db;
