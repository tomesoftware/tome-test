/**
 * @overview Entry point for client API service
 * @author Nick Dedenbach
 * @copyright Tome Software 2018
 */

'use strict';

require('dotenv').config();

// check required ENV vars
const REQUIRED_ENV_VARS = ['SERVER_PORT', 'DB_HOST', 'DB_DATABASE', 'DB_USER', 'DB_PASSWORD', 'NODE_ENV'];
let missing = false;
for (let i = 0; i < REQUIRED_ENV_VARS.length; ++i) {
    if (!(REQUIRED_ENV_VARS[i] in process.env)) {
        console.log(`missing env var ${REQUIRED_ENV_VARS[i]}`);
        missing = true;
    }
}
if (missing) {
    console.log('exiting due to missing env vars');
    process.exit(1);
}


const apiServer = require('./server');

async function startServer() {
    const server = await apiServer;
    await server.start();

    console.log(`Server started at ${server.info.uri}`);
}

startServer();
