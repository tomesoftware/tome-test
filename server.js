/**
 * @overview Starts up pawl-product service
 * @author Nick Dedenbach
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

require('dotenv').config();

const Hapi = require('hapi');

const api = require('./routes');

const server = new Hapi.Server({
    port: process.env.SERVER_PORT || 8040,
});

const registerPlugins = async () => {
    await server.register(api);
};

const init = async () => {
    await registerPlugins();

    return server;
};

process.on('unhandledRejection', err => {
    console.error(err);
    process.exit(1);
});

module.exports = init(); // for testing?
