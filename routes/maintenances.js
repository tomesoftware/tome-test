/**
 * @overview Maintenances handling
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const maintenances = require('../controllers/maintenances');
const userMFDPartMaintenanceModel = require('../models/userMFDPartMaintenance');
const userMFDMaterialMaintenanceModel = require('../models/userMFDMaterialMaintenance');
const {
    MaintenancesQuery,
} = require('../models/query/maintenances');

module.exports.registerMaintenances = (server) => {
    server.route({
        method: 'GET',
        path: '/maintenances',
        handler: maintenances.getMaintenances,
        config: {
            validate: {
                query: MaintenancesQuery,
            },
        },
    });

    server.route({
        method: 'POST',
        path: '/maintenances/mfd/materials',
        handler: maintenances.postMaterialMaintenances,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                },
                payload: userMFDMaterialMaintenanceModel.creationSchema,
            },
        },
    });

    server.route({
        method: 'POST',
        path: '/maintenances/mfd/parts',
        handler: maintenances.postPartMaintenances,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                },
                payload: userMFDPartMaintenanceModel.creationSchema,
            },
        },
    });
};
