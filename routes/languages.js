/**
 * @overview User handling
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const languages = require('../controllers/languages');

module.exports.register = (server) => {
    server.route({
        method: 'GET',
        path: '/languages',
        handler: languages.getLanguages,
    });

    server.route({
        method: 'GET',
        path: '/languages/{language_id}',
        handler: languages.getLanguage,
        config: {
            validate: {
                params: {
                    language_id: Joi.string().guid(),
                },
            },
        },
    });
};
