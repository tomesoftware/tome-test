/**
 * @overview Route glue for the products service
 * @author Nick Dedenbach
 * @copyright Tome Software 2018
 */

'use strict';

const materials = require('../routes/materials');
const parts = require('../routes/parts');
const maintenances = require('../routes/maintenances');
const languages = require('../routes/languages');

async function registerApi(server) {
    materials.registerMaterials(server);
    parts.registerParts(server);
    maintenances.registerMaintenances(server);
    languages.register(server);
}

const plugin = {
    name: 'pawl-product',
    version: '0.0.0',
    register: registerApi,
};

module.exports = plugin;
