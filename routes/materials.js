/**
 * @overview User handling
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const materials = require('../controllers/materials');

// TODO want to write tests that confirm that the correct
// schema has been attached to the correct route.
// e.g., https://stackoverflow.com/questions/48290875/verify-joi-validation-was-added-to-hapi-route
const {
    MFDMaterialsQuery,
    UserMFDMaterialsQuery,
} = require('../models/query/materials');
const {
    UserMFDMaterialAttributesQuery,
} = require('../models/query/attributes');

module.exports.registerMaterials = (server) => {
    server.route({
        method: 'GET',
        path: '/materials/mfd',
        handler: materials.getMFDMaterial,
        config: {
            validate: {
                query: MFDMaterialsQuery,
            },
        },
    });
    server.route({
        method: 'GET',
        path: '/materials/mfd/{mfd_material_id}/name',
        handler: materials.getNameByMFDMaterialId,
        config: {
            validate: {
                query: Joi.object().keys({
                    user_id: Joi.string().guid().required(),
                }),
                params: {
                    mfd_material_id: Joi.string().guid(),
                },
            },
        },
    });
    server.route({
        method: 'GET',
        path: '/materials/mfd/user',
        handler: materials.getMFDMaterialsForUser,
        config: {
            validate: {
                query: UserMFDMaterialsQuery,
            },
        },
    });
    server.route({
        method: 'GET',
        path: '/materials/wheel-sizes',
        handler: materials.getWheelSizes,
    });
    server.route({
        method: 'GET',
        path: '/materials/statuses',
        handler: materials.getStatuses,
    });
    server.route({
        method: 'POST',
        path: '/materials/mfd/user/{mfd_material_id}',
        handler: materials.postMFDMaterialToUser,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                    include_attributes: Joi.boolean(),
                    include_setup: Joi.boolean(),
                },
                params: {
                    mfd_material_id: Joi.string().guid().required(),
                },
                payload: {
                    sensor_serial_number: Joi.string(),
                    user_mfd_part_attributes: Joi.array().items(Joi.object().keys({
                        mfd_part_id: Joi.string(),
                        name: Joi.string(),
                        category: Joi.string(),
                        value: Joi.string(),
                    })),
                },
            },
        },
    });
    server.route({
        method: 'POST',
        path: '/materials/mfd/user',
        handler: materials.postCustomMFDMaterialToUser,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                },
                payload: {
                    serial_number: Joi.string().allow(''),
                    model: Joi.string().required(),
                    manufacturer: Joi.string(),
                    color: Joi.string(),
                    bike_size: Joi.string(),
                    sensor_serial_number: Joi.string(),
                    user_mfd_part_attributes: Joi.array().items(Joi.object().keys({
                        mfd_part_id: Joi.string(),
                        name: Joi.string(),
                        category: Joi.string(),
                        value: Joi.string(),
                    })),
                },
            },
        },
    });
    server.route({
        method: 'DELETE',
        path: '/materials/mfd/user/{user_mfd_material_id}',
        handler: materials.deactivateMFDMaterialForUser,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                },
                params: {
                    user_mfd_material_id: Joi.string().guid().required(),
                },
            },
        },
    });

    server.route({
        method: 'PUT',
        path: '/materials/mfd/user/{user_mfd_material_id}',
        handler: materials.putUserMFDMaterials,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                },
                payload: {
                    user_mfd_material_attributes: Joi.array().items(UserMFDMaterialAttributesQuery),
                    name: Joi.string(),
                    status_id: Joi.string().guid(),
                },
                params: {
                    user_mfd_material_id: Joi.string().guid(),
                },
            },
        },
    });
    server.route({
        method: 'GET',
        path: '/materials/manufacturers',
        handler: materials.getManufacturers,
    });
};
