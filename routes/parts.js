/**
 * @overview Parts handling
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const parts = require('../controllers/parts');

const {
    UserMFDPartsQuery,
} = require('../models/query/parts');

module.exports.registerParts = (server) => {
    server.route({
        method: 'GET',
        path: '/parts/mfd',
        handler: parts.getMFDParts,
    });

    server.route({
        method: 'POST',
        path: '/parts/mfd/user',
        handler: parts.postUserMFDParts,
    });

    server.route({
        method: 'PUT',
        path: '/parts/mfd/user/{id}',
        handler: parts.putUserMFDParts,
        config: {
            validate: {
                query: {
                    user_id: Joi.string().guid().required(),
                },
                payload: UserMFDPartsQuery,
            },
        },
    });
};
