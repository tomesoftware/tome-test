# Pawl Microservice Starter
The Pawl Microservice Starter is a repo meant to include starter code for getting a new microservice repo up quicker. It is opinionated in that it has our official choices for libraries to use across the project on Node.js services (such as Hapi as the web framework, etc.). This is of course up to debate if it does not fit the design of the new service you are generating.

## General Guidelines
### Module Importing
The `require` method employed by Node for module importing will look for an `index.js` file and import it if you give a folder path as the argument. This is an easier way to compose an API out of several pieces and then make one export. Use this method unless absolutely not possible. A simple example can be found in the `routes` folder. An `index.js` file there brings in a `users.js` file and exposes it on its export.

### When In Doubt
Refer to the [Tomobiliteam repo](https://bitbucket.org/tomesoftware/tomobiliteam/src/master/), which contains general practices the team tries to employ. Abide by those guidelines for anything not explicitly covered here.

## Enviroment Variables
Environment variables are used to define specific configuration based on the environment the service is running in. The default `server.js` will load in any environment variables defined in a `.env` file (not included). The example app in this repo used the following environment variables as an example. Any value in parentheses is what the application will use by default if not environment variable is detected.

* SERVER_PORT (8080)

## File Structure
We want to keep a loosely consistent file structure throughout the project in order to keep things cleaner and make peer reviewing unfamiliar services a bit quicker. With that in mind, a number of example folders and files are defined in this repo, which we will quickly summarize below.

### Top Level Files
* `.dockerignore` - The files and folders Docker will ignore when creating an image with the `Dockerfile`
* `.gitignore` - The files and folders Git will ignore when looking for changes
* `app.js` - The entry point for the service, which in simple cases should just be starting up the Hapi server
* `Dockerfile` - The file [used by Docker](https://docs.docker.com/engine/reference/builder/) to create an image for this service
* `gulpfile.js` - The top-level [Gulp](https://gulpjs.org/) file, the one actually responsible for creatings the tasks when a `gulp` command is run
* `package.json` - The JSON file that contains metadata for the service and all the package dependencies the service needs to run
* `postman-examples.json` - An example Postman export file that can be imported into Postman to see an example of defined requests
* `redeploy.sh` - A Bash script that can be used to rebuild the Docker image and run it (with some tweaking)
* `server.js` - The server definition file; all Hapi registration should happen in here

### controllers
This folder should include all functions that can be used as the handler for a Hapi route definition. See the [Hapi documentation](https://hapijs.com/api) for more details on what a route handler does and when it is fired in the overall Hapi lifecycle.

NOTE: All handlers for routes registered by plugins should be defined in the plugins folder, not here.

### gulp-tasks
The `gulp-tasks` should contains a list of scripts that define the repo's Gulp tasks. The default tasks are defined in detail in the `Tools` section below.

NOTE: These files are referenced in the top-level Gulpfile to actually define the task. So if you create a new Gulp task for your repo here, it must also be set up properly in the top-level file.

### helpers
The `helpers` folder includes all helpers and utilites. A token generation/validation library is included here as an example to follow.

### models
The `models` folder should include all of the Joi validation schemas that need to be used in this specific microservice. An example for a user model is included. The module export definition for any new model files should reflect the one found in that example - each model gets its own field in the export object, with a capitalized name. Please look into (Joi)[https://github.com/hapijs/joi] for more information and help on how to build good validation schemas and how to use them to validation data.

### plugins
The `plugins` folder is meant to hold plugin definitions that we need for the Hapi server at the core of the microservice. A plugin in Hapi parlance is simply a reusable bit of logic that takes a Hapi server and registers hooks and other methods. For example, in this starter repo is an authentication plugin that registers an authentication strategy and some routes. If your service does need authentication, you can delete this, it is only included as a template.

### routes
The `routes` folder should hold all miscellaneous route definition required for the server. By miscellaneous, we mean any routes that are not encapsulated in the custom plugins for the repo.

### test
The `test` folder should include all Ava test files. The test folder should do its best to reflect the structure of the repo it contains tests for. So if a repo has a `controllers` folder, there should be a `controllers` folder in the top level of the `test` folder as well, where all those associated tests are kept.

## Tools
### Hapi 
Our server framework of choice is [Hapi](https://hapijs.com/api).

### Gulp
Gulp is the build system automation software used in this service repo. It has the following tasks -

* Ava - takes source files and executes Ava tests with NYC (Istanbul) code coverage provided by default. Modification would have to be done to remove code coverage. Ava configuration is located in the package.json of this file under *ava*; the two main options of interest are sources (files being tested) and files (the test files themselves). You can enter the following CLI options:
	* --ff - Stops on first test failure

* ESLint - a linter task that uses a modified Airbnb style guide. See the guide
[here](https://github.com/airbnb/javascript). Important modifications are as follows:
    * Whitespace - 4 spaces instead of 2
    * Shadowed variables - allowed variables with the names "cb", "err", or "done" to be covered
    * Script type - tells the linter to consider the files basic scripts, not module definitions.
    This is because ES6 module support in Node.js is still iffy.
    * Max length of line - raised to 100
It also supports the following CLI flags:
    * --fix - Will try to automatically fix the more straightforward rules (missing semicolons, etc.)

The configuration, complete with base style guide and the above listed modifications, is available as the *eslintConfig* option in package.json. The files that are looked at for linting are also defined in this config.

* test - Runs the linter and then the Ava test runner.

### Postman
Postman is a tool for simplifying the development of APIs. With that in mind, this service has example API calls from Postman exported into a JSON file at `postman-examples.json`. Utilize this whenever you would like to make requests independent from a client by importing the JSON file into the [Postman app](https://www.getpostman.com/).

As far as the client API is concerned, session management should be handled by Postman automatically. It will store all cookies sent to it, so as long as the user hits the login request before any other requests, they should be good to go.

### Docker
We utilize Docker to quickly spin up containers of each service for local development and possibly QA/production deployment. With that in mind, we always want a `Dockerfile` defined for a service based on this repo. A pretty generic example is included in the top level of this directory that simply takes `app.js` and runs it using `nodemon.

## Testing
For testing, we utilize the Ava test framework, as well as the Sinon object mocking/spying library along with other various utility libraries. Please see developer dependencies in the `package.json` if you would like to know what those other utilities are.

### Advice
Testing always comes with some quirks. We will do our best to summarize our advice here.

#### General
Whenever possible, attach common resources to the Ava context object. This context object is passed to all tests and hooks, making it easier to build up commonly used objects in tests (for example, a Hapi server object). By setting up such state in the hooks themselves and not in the global context, we deal with potential weird scoping issues. Below is one example of creating a Hapi server and attaching it to the context for future use.

```javascript
test.beforeEach(t => {
    t.context.server = new Hapi.Server({
        autoListen: false,
    });
});

test('server exists', async t => {
    const { server } = t.context;
    t.true(typeof server === 'object');
});
```

#### Hapi
Hapi's main structure is the server object. Hapi, very helpfully, offers an [`inject` method](https://hapijs.com/api#-await-serverinjectoptions) on this object that allows the user to directly inject a request simulating HTTP requests without making an actual connection. Please use this for any end-to-end testing on the server itself - this decreases the overhead of running a bunch of them in parallel.

When creating a server object within the test itself, without the intention of opening up a connection or simulating a full request, one should set the `autoListen` property on the options object to `false`. This prevents the server from even opening up a connection, which will make running Ava tests in parallel much simpler.

```javascript
const server = new Hapi.Server({
    autoListen: false,
});
```
