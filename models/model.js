/**
 * @overview Model model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const Platform = require('./part');
const Resource = require('./resource');

const modelBaseSchema = Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
});

const modelCreationSchema = modelBaseSchema.keys({
    platform: Platform.creationSchema,
});


const modelSchema = modelBaseSchema.keys({
    model_id: Joi.string().uuid().required(),
    platform: Platform.schema,
    resources: Joi.array().items(Resource.schema),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, modelSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            model_id: databaseObject.dataValues.ModelId,
            name: databaseObject.dataValues.Name,
            description: databaseObject.dataValues.Description,
        };


        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            apiObject = validate(apiObject);
        }
        // We do this after validation because materials aren't in the schema
        // to avoid circular dependancies
        // eslint-disable-next-line global-require
        apiObject.materials = databaseObject.dataValues.Materials.map((m) => require('./material').serialize.databaseToApi(m));
        return apiObject;
    },
};


module.exports = {
    creationSchema: modelCreationSchema,
    schema: modelSchema,
    validate: validate,
    serialize: serialize,
};
