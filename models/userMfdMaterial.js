/**
 * @overview userMFDMaterial model
 * @author Angela Fessler
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const Attribute = require('./attribute');
const Part = require('./part');
const MFDPart = require('./mfdPart');
const UserMFDPart = require('./userMfdPart');
const MFDMaterial = require('./mfdMaterial');
const Status = require('./status');

const userMFDMaterialBaseSchema = Joi.object().keys({
    name: Joi.string(),
    actual_circumference: Joi.number(),
    crm_id: Joi.string().uuid(),
});

const userMFDMaterialCreationSchema = userMFDMaterialBaseSchema.keys({
    mfd_material: MFDMaterial.creationSchema.required(),
    mfd_parts: Joi.array().items(MFDPart.creationSchema),
});

const userMFDMaterialSchema = userMFDMaterialBaseSchema.keys({
    user_mfd_material_id: Joi.string().uuid().required(),
    user_id: Joi.string().uuid().required(),
    mfd_material: MFDMaterial.schema.required(),
    parts: Joi.array().items(Part.schema),
    user_mfd_parts: Joi.array().items(UserMFDPart.schema),
    attributes: Joi.array().items(Attribute.schema),
    status: Status.schema.required(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, userMFDMaterialSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            user_mfd_material_id: databaseObject.dataValues.UserMFDMaterialId,
            user_id: databaseObject.dataValues.UserId,
            crm_id: databaseObject.dataValues.CrmId,
            name: databaseObject.dataValues.Name,
            mfd_material: MFDMaterial.serialize.databaseToApi(databaseObject.MFDMaterial, false),
            status: Status.serialize.databaseToApi(databaseObject.Status, false),
        };

        if (databaseObject.Parts && databaseObject.Parts.length > 0) {
            apiObject.parts = [];
            databaseObject.Parts.forEach(part => {
                apiObject.parts.push(
                    Part.serialize.databaseToApi(part.Part, false),
                );
            });
        }

        if (databaseObject.UserMFDParts && databaseObject.UserMFDParts.length > 0) {
            apiObject.user_mfd_parts = [];
            databaseObject.UserMFDParts.forEach(userMFDPart => {
                const finalMfdPart = UserMFDPart.serialize.databaseToApi(userMFDPart.UserMFDPart, false);
                finalMfdPart.user_mfd_part_id = userMFDPart.UserMFDPartId;
                apiObject.user_mfd_parts.push(
                    finalMfdPart,
                );
            });
        }

        if (databaseObject.UserMFDMaterialAttributes) {
            apiObject.attributes = databaseObject.UserMFDMaterialAttributes.map(attribute => Attribute.serialize.databaseToApi(attribute, false));
        }

        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    creationSchema: userMFDMaterialCreationSchema,
    schema: userMFDMaterialSchema,
    validate: validate,
    serialize: serialize,
};
