/**
 * @overview Manufacturer model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const ModelModel = require('./model');

const manufacturerBaseSchema = Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
});

const manufacturerCreationSchema = manufacturerBaseSchema.keys();

const manufacturerSchema = manufacturerBaseSchema.keys({
    manufacturer_id: Joi.string().uuid().required(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, manufacturerSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            manufacturer_id: databaseObject.dataValues.ManufacturerId,
            name: databaseObject.dataValues.Name,
            description: databaseObject.dataValues.Description,
        };


        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            apiObject = validate(apiObject);
        }
        // This is below validation because it can't go in the schema due to
        // circular depandancies.
        apiObject.models = databaseObject.dataValues.Platforms.map((p) => p.dataValues.Models.map((m) => ModelModel.serialize.databaseToApi(m)));
        return apiObject;
    },
};

module.exports = {
    creationSchema: manufacturerCreationSchema,
    schema: manufacturerSchema,
    serialize: serialize,
    validate: validate,
};
