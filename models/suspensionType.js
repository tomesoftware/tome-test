/**
 * @overview SuspensionType model
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

const Joi = require('joi');
const Boom = require('boom');
const _ = require('lodash');

const SuspensionSetUp = require('./suspensionSetUp');

const suspensionTypeSchema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string(),
    setups: Joi.array().items(SuspensionSetUp.schema),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, suspensionTypeSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            name: databaseObject.Name,
            description: databaseObject.Description,
        };

        if (databaseObject.SuspensionSetUps) {
            apiObject.setups = databaseObject.SuspensionSetUps.map(
                setup => SuspensionSetUp.serialize.databaseToApi(setup, false),
            );
        }

        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    schema: suspensionTypeSchema,
    validate: validate,
    serialize: serialize,
};
