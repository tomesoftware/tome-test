/**
 * @overview Parts request query model
 * @author Ben Willshire
 * @copyright Tome Software 2019
 */

'use strict';

const Joi = require('joi');
const {
    UserMFDPartAttributesQuery,
} = require('./attributes');

const UserMFDPartsQuerySchema = Joi.object().keys({
    user_mfd_part_attributes: Joi.array().items(UserMFDPartAttributesQuery),
});

module.exports = {
    UserMFDPartsQuery: UserMFDPartsQuerySchema,
};
