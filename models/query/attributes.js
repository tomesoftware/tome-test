/**
 * @overview Attributes request query model
 * @author Ben Willshire
 * @copyright Tome Software 2019
 */

'use strict';

const Joi = require('joi');

const UserMFDPartAttributesQuerySchema = Joi.object().keys({
    user_mfd_part_attribute_id: Joi.string().guid(),
    name: Joi.string(),
    category: Joi.string(),
    value: Joi.string().required().allow(''),
    display_order: Joi.number(),
});

const UserMFDMaterialAttributesQuerySchema = Joi.object().keys({
    user_mfd_material_attribute_id: Joi.string().guid(),
    name: Joi.string(),
    category: Joi.string(),
    value: Joi.string().required().allow(''),
    display_order: Joi.number(),
});

module.exports = {
    UserMFDPartAttributesQuery: UserMFDPartAttributesQuerySchema,
    UserMFDMaterialAttributesQuery: UserMFDMaterialAttributesQuerySchema,
};
