/**
 * @overview Materials request query model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');


const MFDMaterialsQuerySchema = Joi.object().keys({
    sensor_serial_number: Joi.string(),
    bike_serial_number: Joi.string(),
    include_parts: Joi.boolean(),
    include_resources: Joi.boolean(),
    include_setup: Joi.boolean(),
    include_attributes: Joi.boolean(),
    include_user_created: Joi.boolean(),
})
    .without('sensor_serial_number', ['bike_serial_number'])
    .without('bike_serial_number', ['sensor_serial_number']);

const UserMFDMaterialsQuerySchema = Joi.object().keys({
    user_id: Joi.string().guid().required(),
    include_parts: Joi.boolean(),
    include_resources: Joi.boolean(),
    include_attributes: Joi.boolean(),
    include_setup: Joi.boolean(),
});

module.exports = {
    MFDMaterialsQuery: MFDMaterialsQuerySchema,
    UserMFDMaterialsQuery: UserMFDMaterialsQuerySchema,
};
