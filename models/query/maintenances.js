/**
 * @overview Services request query model
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');


const ServicesQuerySchema = Joi.object().keys({
    user_id: Joi.string().guid().required(),
    user_mfd_material_id: Joi.string().guid().required(),
    include_performed: Joi.boolean(),
});

module.exports = {
    ServicesQuery: ServicesQuerySchema,
};
