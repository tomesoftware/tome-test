/**
 * @overview Manufcatured part assigned to a user model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const Attribute = require('./attribute');
const MFDPart = require('./mfdPart');

const userMfdPartCreationSchema = Joi.object().keys({
    mfd_part: MFDPart.creationSchema.required(),
});

const userMfdPartSchema = Joi.object().keys({
    user_mfd_part_id: Joi.string().required(),
    mfd_part: MFDPart.schema.required(),
    attributes: Joi.array().items(Attribute.schema),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, userMfdPartSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            user_mfd_part_id: databaseObject.dataValues.MFDPartId,
            mfd_part: MFDPart.serialize.databaseToApi(databaseObject.MFDPart, false),
        };
        if (databaseObject.UserMFDPartAttributes) {
            apiObject.attributes = databaseObject.UserMFDPartAttributes.map(userMFDPartAttribute => Attribute.serialize.databaseToApi(userMFDPartAttribute, false));
        }
        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    creationSchema: userMfdPartCreationSchema,
    schema: userMfdPartSchema,
    serialize: serialize,
    validate: validate,
};
