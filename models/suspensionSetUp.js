/**
 * @overview SuspensionType model
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

const Joi = require('joi');
const Boom = require('boom');
const _ = require('lodash');

const suspensionSetUpSchema = Joi.object().keys({
    rider_weight: Joi.number().integer().required(),
    psi: Joi.number().integer().required(),
    clicks: Joi.number().integer(),
    coil: Joi.number().integer(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, suspensionSetUpSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            rider_weight: databaseObject.RiderWeightInPounds,
            psi: databaseObject.PSI,
            clicks: databaseObject.Clicks,
            coil: databaseObject.Coil,
        };

        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    schema: suspensionSetUpSchema,
    validate: validate,
    serialize: serialize,
};
