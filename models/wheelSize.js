/**
 * @overview WheelSize model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const Boom = require('boom');
const _ = require('lodash');

const wheelSizeBaseSchema = Joi.object().keys({
    name: Joi.string(),
    description: Joi.string().allow('', null),
    wheel_size: Joi.number().allow(null),
});

const wheelSizeCreationSchema = wheelSizeBaseSchema.keys();

const wheelSizeSchema = wheelSizeBaseSchema.keys({
    wheel_size_id: Joi.string().uuid().required(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, wheelSizeSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject) => {
        let apiObject = {
            wheel_size_id: databaseObject.WheelSizeId,
            name: databaseObject.Name,
            description: databaseObject.Description,
            wheel_size: databaseObject.WheelSize,
        };
        apiObject = _.omitBy(apiObject, _.isNull);
        apiObject = _.omitBy(apiObject, _.isUndefined);
        return apiObject;
    },
};

module.exports = {
    creationSchema: wheelSizeCreationSchema,
    schema: wheelSizeSchema,
    validate: validate,
    serialize: serialize,
};
