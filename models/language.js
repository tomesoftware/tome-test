/**
 * @overview Language model
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const resourceLanguageSchema = Joi.object().keys({
    language_id: Joi.string().uuid().required(),
    name: Joi.string().required(),
    description: Joi.string().allow(''),
});

function validate(apiObject, schema) {
    try {
        return Joi.attempt(apiObject, schema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            language_id: databaseObject.ResourceLanguageId,
            name: databaseObject.Name,
            description: databaseObject.Description,
        };
        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject, resourceLanguageSchema);
        }
        return apiObject;
    },
};

module.exports = {
    schema: resourceLanguageSchema,
    serialize: serialize,
    validate: validate,
};
