/**
 * @overview attribute model
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const attributeSchema = Joi.object().keys({
    user_mfd_material_attribute_id: Joi.string(),
    user_mfd_part_attribute_id: Joi.string(),
    material_attribute_id: Joi.string(),
    part_attribute_id: Joi.string(),
    display_order: Joi.number(),
    name: Joi.string().required(),
    category: Joi.string().required(),
    value: Joi.string().required().allow(''),
    user_mfd_material_attribute_description: Joi.string(),
    user_mfd_part_attribute_description: Joi.string(),
    material_attribute_description: Joi.string(),
    part_attribute_description: Joi.string(),
    attribute_description: Joi.string(),
    attribute_category_description: Joi.string(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, attributeSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            user_mfd_material_attribute_id: databaseObject.dataValues.UserMFDMaterialAttributeId,
            user_mfd_part_attribute_id: databaseObject.dataValues.UserMFDPartAttributeId,
            material_attribute_id: databaseObject.dataValues.MaterialAttributeId,
            part_attribute_id: databaseObject.dataValues.PartAttributeId,
            name: databaseObject.Attribute.dataValues.Name,
            category: databaseObject.Attribute.AttributeCategory.dataValues.Name,
            value: databaseObject.dataValues.Value,
            display_order: databaseObject.dataValues.DisplayOrder,
            user_mfd_material_attribute_description: databaseObject.dataValues.UserMFDMaterialAttributeDescription,
            user_mfd_part_attribute_description: databaseObject.dataValues.UserMFDPartAttributeDescription,
            material_attribute_description: databaseObject.dataValues.MaterialAttributeDescription,
            part_attribute_description: databaseObject.dataValues.PartAttributeDescription,
            attribute_description: databaseObject.Attribute.dataValues.Description,
            attribute_category_description: databaseObject.Attribute.AttributeCategory.dataValues.Description,
        };
        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    schema: attributeSchema,
    serialize: serialize,
    validate: validate,
};
