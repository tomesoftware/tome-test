/**
 * @overview resource model
 * @author Angela Fessler
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const resourceCoreSchema = Joi.object().keys({
    resource_id: Joi.string().required(),
    name: Joi.string().required(),
    description: Joi.string().allow(''),
    type: Joi.string().required(),
    category: Joi.string().required(),
    url: Joi.string().required(),
    version: Joi.string().allow(''),
    language: Joi.string().required(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, resourceCoreSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            resource_id: databaseObject.dataValues.ResourceId,
            name: databaseObject.Resource.dataValues.Name,
            description: databaseObject.Resource.dataValues.Description,
            type: databaseObject.Resource.ResourceType.dataValues.Name,
            category: databaseObject.Resource.ResourceCategory.dataValues.Name,
            url: databaseObject.Resource.dataValues.URL,
            version: databaseObject.Resource.dataValues.Version,
            language: databaseObject.Resource.ResourceLanguage.dataValues.Name,
        };
        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    schema: resourceCoreSchema,
    serialize: serialize,
    validate: validate,
};
