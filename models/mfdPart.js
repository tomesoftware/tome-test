/**
 * @overview Manufactured part model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const Attribute = require('./attribute');
const Part = require('./part');

const mfdPartBaseSchema = Joi.object().keys({
    serial_number: Joi.string(),
});

const mfdPartCreationSchema = mfdPartBaseSchema.keys({
    part: Part.creationSchema.required(),
});

const mfdPartSchema = mfdPartBaseSchema.keys({
    mfd_part_id: Joi.string().required(),
    part: Part.schema.required(),
    attributes: Joi.array().items(Attribute.schema),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, mfdPartSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            mfd_part_id: databaseObject.dataValues.MFDPartId,
            serial_number: databaseObject.dataValues.SerialNumber,
            part: Part.serialize.databaseToApi(databaseObject.Part.dataValues),
        };
        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    creationSchema: mfdPartCreationSchema,
    schema: mfdPartSchema,
    serialize: serialize,
    validate: validate,
};
