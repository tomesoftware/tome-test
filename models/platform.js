/**
 * @overview Platform model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
// const _ = require('lodash');
const Boom = require('boom');

const Manufacturer = require('./manufacturer');

const platformBaseSchema = Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
});

const platformCreationSchema = platformBaseSchema.keys({
    manufacturer: Manufacturer.creationSchema,
});

const platformSchema = platformBaseSchema.keys({
    platform_id: Joi.string().uuid().required(),
    manufacturer: Manufacturer.schema,
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, platformSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

module.exports = {
    creationSchema: platformCreationSchema,
    schema: platformSchema,
    validate: validate,
};
