/**
 * @overview UserMFDMaterialMaintenance model
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');

const creationSchema = Joi.object().keys({
    user_mfd_material_id: Joi.string().guid().required(),
    model_maintenance_id: Joi.string().guid().required(),
    notes: Joi.string(),
    odometer: Joi.number(),
    service_date: Joi.date(),
    dealer_id: Joi.string(),
});

const serialize = {
    databaseToApi: (databaseObject) => ({
        active: databaseObject.Active,
        user_mfd_material_maintenance_id: databaseObject.UserMFDMaterialMaintenanceId,
        user_mfd_material_id: databaseObject.UserMFDMaterialId,
        model_maintenance_id: databaseObject.ModelMaintenanceId,
        notes: databaseObject.Notes,
        odometer: databaseObject.Odometer,
        service_date: databaseObject.ServiceDate,
        dealer_id: databaseObject.DealerId,
        date_updated: databaseObject.DateUpdated,
        date_created: databaseObject.DateCreated,
    }),
};

module.exports = {
    creationSchema: creationSchema,
    serialize: serialize,
};
