/**
 * @overview material model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const MFDPart = require('./mfdPart');
const Material = require('./material');

const mfdMaterialBaseSchema = Joi.object().keys({
    serial_number: Joi.string().allow(''),
});

const mfdMaterialCreationSchema = mfdMaterialBaseSchema.keys({
    material: Material.creationSchema,
    mfd_parts: Joi.array().items(MFDPart.creationSchema),
});

const mfdMaterialSchema = mfdMaterialBaseSchema.keys({
    mfd_material_id: Joi.string().required(),
    material: Material.schema,
    mfd_parts: Joi.array().items(MFDPart.schema),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, mfdMaterialSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {

    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            mfd_material_id: databaseObject.dataValues.MFDMaterialId,
            serial_number: databaseObject.dataValues.SerialNumber,
            material: Material.serialize.databaseToApi(databaseObject.Material, false),
        };
        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    creationSchema: mfdMaterialCreationSchema,
    schema: mfdMaterialSchema,
    serialize: serialize,
    validate: validate,
};
