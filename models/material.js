/**
 * @overview material model
 * @author Angela Fessler
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const Attribute = require('./attribute');
const Part = require('./part');
const Model = require('./model');
const Resource = require('./resource');

const materialBaseSchema = Joi.object().keys({
    name: Joi.string().required(),
    material_number: Joi.string(),
    description: Joi.string(),
    color: Joi.string().allow(''),
    frame_size: Joi.string().allow(''),
    image_url: Joi.string(),
    attributes: Joi.array().items(Attribute.schema),
});

const materialCreationSchema = materialBaseSchema.keys({
    model: Model.creationSchema,
    parts: Joi.array().items(Part.creationSchema),
});


const materialSchema = materialBaseSchema.keys({
    material_id: Joi.string().uuid().required(),
    model: Joi.string(),
    platform: Joi.string(),
    manufacturer: Joi.string(),
    tire_size: Joi.string(),
    wheel_size: Joi.string(),
    circumference: Joi.number(),
    resources: Joi.array().items(Resource.schema),
    parts: Joi.array().items(Part.schema),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, materialSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            material_id: databaseObject.dataValues.MaterialId,
            name: databaseObject.dataValues.Name,
            material_number: databaseObject.dataValues.MaterialNumber,
            color: databaseObject.dataValues.Color,
            frame_size: databaseObject.dataValues.FrameSize,
            image_url: databaseObject.dataValues.ImageURL,
        };

        if (databaseObject.MaterialAttributes) {
            apiObject.attributes = databaseObject.MaterialAttributes.map(materialAttribute => Attribute.serialize.databaseToApi(materialAttribute, false));
        }

        apiObject.resources = [];

        if (databaseObject.Model) {
            apiObject.model = databaseObject.Model.dataValues.Name;
            if (databaseObject.Model.Platform) {
                apiObject.manufacturer = databaseObject.Model.Platform.Manufacturer.dataValues.Name;
                apiObject.platform = databaseObject.Model.Platform.dataValues.Name;
            }

            const modelResources = databaseObject.Model.ModelResources;
            if (modelResources) {
                modelResources.forEach(resource => {
                    apiObject.resources.push(Resource.serialize.databaseToApi(resource, false));
                });
            }
        }

        const materialResources = databaseObject.MaterialResources;
        if (materialResources) {
            materialResources.forEach(resource => {
                apiObject.resources.push(Resource.serialize.databaseToApi(resource, false));
            });
        }


        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    creationSchema: materialCreationSchema,
    schema: materialSchema,
    serialize: serialize,
    validate: validate,
};
