/**
 * @overview part model
 * @author Angela Fessler
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const Attribute = require('./attribute');
const Resource = require('./resource');
const SuspensionType = require('./suspensionType');

const partBaseSchema = Joi.object().keys({
    name: Joi.string().required(),
    model_number: Joi.string(),
    part_type: Joi.string().required(),
    attributes: Joi.array().items(Attribute.schema),
});

const partCreationSchema = partBaseSchema.keys();

const partSchema = partBaseSchema.keys({
    part_id: Joi.string().uuid().required(),
    resources: Joi.array().items(Resource.schema),
    suspension_type: SuspensionType.schema,
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, partSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        let apiObject = {
            part_id: databaseObject.PartId,
            name: databaseObject.Name,
            model_number: databaseObject.ModelNumber,
            part_type: databaseObject.PartType.dataValues.Name,
        };

        const partResources = databaseObject.PartResources;
        const suspensionTypes = databaseObject.SuspensionTypes;

        if (databaseObject.PartAttributes) {
            apiObject.attributes = databaseObject.PartAttributes.map(partAttribute => Attribute.serialize.databaseToApi(partAttribute, false));
        }

        if (partResources && partResources.length > 0) {
            apiObject.resources = [];
            partResources.forEach(resource => {
                apiObject.resources.push(Resource.serialize.databaseToApi(resource, false));
            });
        }

        if (suspensionTypes && suspensionTypes.length > 0) {
            apiObject.suspension_type = SuspensionType.serialize.databaseToApi(suspensionTypes[0]);
        }

        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    creationSchema: partCreationSchema,
    schema: partSchema,
    serialize: serialize,
    validate: validate,
};
