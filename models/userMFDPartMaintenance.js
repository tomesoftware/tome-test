/**
 * @overview UserMFDPartMaintenance model
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');

const creationSchema = Joi.object().keys({
    user_mfd_part_id: Joi.string().guid().required(),
    part_maintenance_id: Joi.string().guid().required(),
    notes: Joi.string(),
    odometer: Joi.number().min(0),
    service_date: Joi.date(),
    dealer_id: Joi.string(),
});

const serialize = {
    databaseToApi: (databaseObject) => ({
        active: databaseObject.Active,
        user_mfd_part_maintenance_id: databaseObject.UserMFDPartMaintenanceId,
        user_mfd_part_id: databaseObject.UserMFDPartId,
        part_maintenance_id: databaseObject.PartMaintenanceId,
        notes: databaseObject.Notes,
        odometer: databaseObject.Odometer,
        service_date: databaseObject.ServiceDate,
        dealer_id: databaseObject.DealerId,
        date_updated: databaseObject.DateUpdated,
        date_created: databaseObject.DateCreated,
    }),
};

module.exports = {
    creationSchema: creationSchema,
    serialize: serialize,
};
