/**
 * @overview Status model
 * @copyright Tome Software 2018
 */

'use strict';

const Joi = require('joi');
const _ = require('lodash');
const Boom = require('boom');

const statusSchema = Joi.object().keys({
    status_id: Joi.string(),
    name: Joi.string(),
    description: Joi.string(),
});

function validate(apiObject) {
    try {
        return Joi.attempt(apiObject, statusSchema);
    } catch (e) {
        throw Boom.badRequest(e.details[0].message);
    }
}

const serialize = {
    databaseToApi: (databaseObject, shouldValidate = true) => {
        if (!databaseObject) {
            return {
                name: 'Unknown',
                description: 'Unknown',
            };
        }
        let apiObject = {
            status_id: databaseObject.dataValues ? databaseObject.dataValues.StatusId : databaseObject.StatusId,
            name: databaseObject.dataValues ? databaseObject.dataValues.Name : databaseObject.Name,
            description: databaseObject.dataValues ? databaseObject.dataValues.Description : databaseObject.Description,
        };

        apiObject = _.omitBy(apiObject, _.isNull);
        if (shouldValidate) {
            return validate(apiObject);
        }
        return apiObject;
    },
};

module.exports = {
    schema: statusSchema,
    validate: validate,
    serialize: serialize,
};
