/**
 * @overview Server creation tests
 * @author Nick Dedenbach
 * @copyright Tome Software 2018
 */

'use strict';

const test = require('ava');

const server = require('../server');

test('server object exists', async t => {
    const resolvedServer = await server;
    t.true(typeof resolvedServer === 'object');
});
