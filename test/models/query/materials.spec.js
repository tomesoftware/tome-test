/**
 * @overview Materials query joi models unit tests
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const test = require('ava');
const chance = require('chance').Chance();
const Joi = require('joi');

const {
    MFDMaterialsQuery,
    UserMFDMaterialsQuery,
} = require('../../../models/query/materials');

async function validateSchema(value, schema, t, isValid) {
    try {
        await Joi.validate(value, schema);

        return (isValid || isValid === undefined)
            ? t.pass()
            : t.fail('Schema validation passed when it should not have.');
    } catch (e) {
        return (isValid || isValid === undefined)
            ? t.fail(`Schema validation failed when it should not have: ${e.message}`)
            : t.pass();
    }
}


test.before(async t => {
    t.context.valid = {
        sensorSerialNumber: { sensor_serial_number: '230582025-3q2512' },
        bikeSerialNumber: { bike_serial_number: '230582025-3q2512' },
        includeResources: { include_resources: 'true' },
        includeParts: { include_parts: 'true' },
        userId: { user_id: chance.guid({ version: 4 }) },
        id: { id: chance.guid({ version: 4 }) },
    };

    t.context.invalid = {
        sensorSerialNumber: { sensor_serial_number: null },
        bikeSerialNumber: { bike_serial_number: null },
        includeResources: { include_resources: 'Y' },
        includeParts: { include_parts: 'Y' },
        userId: { user_id: 8 },
        id: { id: 7 },
    };

    t.context.undef = {
        sensorSerialNumber: { sensor_serial_number: undefined },
        bikeSerialNumber: { bike_serial_number: undefined },
        includeResources: { include_resources: undefined },
        includeParts: { include_parts: undefined },
        userId: { user_id: undefined },
        id: { id: undefined },
    };

    t.context.nil = {
        sensorSerialNumber: { sensor_serial_number: null },
        bikeSerialNumber: { bike_serial_number: null },
        includeResources: { include_resources: null },
        includeParts: { include_parts: null },
        userId: { user_id: null },
        id: { id: null },
    };
});

/* MFDMaterialsQuery */
test('MFDMaterialsQuery schema exists', async t => {
    t.truthy(MFDMaterialsQuery);
});

test('MFDMaterialsQuery schema accepts a valid object', async t => {
    const query = Object.assign(
        t.context.valid.sensorSerialNumber,
        t.context.valid.includeResources,
        t.context.valid.includeParts,
    );

    await validateSchema(query, MFDMaterialsQuery, t);
});

test('MFDMaterialsQuery schema rejects if both sensor_serial_number and bike_serial_number exist', async t => {
    const query = Object.assign(
        t.context.valid.sensorSerialNumber,
        t.context.valid.bikeSerialNumber,
    );

    await validateSchema(query, MFDMaterialsQuery, t, false);
});

test('MFDMaterialsQuery schema accepts empty object', async t => {
    await validateSchema({}, MFDMaterialsQuery, t);
});

test('MFDMaterialsQuery schema rejects if include_parts is not boolean string', async t => {
    const queries = [
        Object.assign(t.context.invalid.includeParts),
        Object.assign(t.context.nil.includeParts),
    ];
    await Promise.all(queries.map(q => validateSchema(q, MFDMaterialsQuery, t, false)));
});

test('MFDMaterialsQuery schema rejects if include_resources is not boolean string', async t => {
    const queries = [
        Object.assign(t.context.invalid.includeResources),
        Object.assign(t.context.nil.includeResources),
    ];
    await Promise.all(queries.map(q => validateSchema(q, MFDMaterialsQuery, t, false)));
});

test('MFDMaterialsQuery schema rejects if sensor_serial_number is invalid', async t => {
    const queries = [
        Object.assign(t.context.invalid.sensorSerialNumber),
        Object.assign(t.context.nil.sensorSerialNumber),
    ];
    await Promise.all(queries.map(q => validateSchema(q, MFDMaterialsQuery, t, false)));
});

test('MFDMaterialsQuery schema rejects if bike_serial_number is invalid', async t => {
    const queries = [
        Object.assign(t.context.invalid.bikeSerialNumber),
        Object.assign(t.context.nil.bikeSerialNumber),
    ];
    await Promise.all(queries.map(q => validateSchema(q, MFDMaterialsQuery, t, false)));
});

/* UserMFDMaterialsQuery */
test('UserMFDMaterialsQuery schema exists', async t => {
    t.truthy(UserMFDMaterialsQuery);
});

test('UserMFDMaterialsQuery schema accepts valid schema', async t => {
    const query = Object.assign(
        t.context.valid.userId,
    );
    await validateSchema(query, UserMFDMaterialsQuery, t);
});

test('UserMFDMaterialsQuery schema accepts valid schema with include options', async t => {
    const query = Object.assign(
        t.context.valid.userId,
        t.context.valid.includeParts,
        t.context.valid.includeResources,
    );
    await validateSchema(query, UserMFDMaterialsQuery, t);
});

test('UserMFDMaterialsQuery schema rejects if user_id is null or undefined', async t => {
    const queries = [
        Object.assign(t.context.nil.userId),
        Object.assign(t.context.undef.userId),
    ];
    await Promise.all(queries.map(q => validateSchema(q, UserMFDMaterialsQuery, t, false)));
});

test('UserMFDMaterialsQuery schema rejects if include_parts is null or invalid', async t => {
    const queries = [
        Object.assign(t.context.valid.userId, t.context.nil.includeParts),
        Object.assign(t.context.valid.userId, t.context.invalid.includeParts),
    ];
    await Promise.all(queries.map(q => validateSchema(q, UserMFDMaterialsQuery, t, false)));
});

test('UserMFDMaterialsQuery schema rejects if include_resources is null or invalid', async t => {
    const queries = [
        Object.assign(t.context.valid.userId, t.context.nil.includeResources),
        Object.assign(t.context.valid.userId, t.context.invalid.includeResources),
    ];
    await Promise.all(queries.map(q => validateSchema(q, UserMFDMaterialsQuery, t, false)));
});
