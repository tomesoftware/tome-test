/**
 * @overview Materials controller unit tests
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const test = require('ava');
const chance = require('chance').Chance();

const materials = require('../../controllers/materials');

test.beforeEach(async t => {
    t.context.request = {
        params: {
            id: chance.guid(),
        },
        query: {
            user_id: chance.guid(),
        },
    };
});

test('getMFDMaterials exists', async t => {
    t.truthy(materials.getMFDMaterial);
});

test('getMFDMaterials rejects if no sensor_serial_number or bike_serial_number is sent', async t => {
    const { request } = t.context;
    await t.throwsAsync(materials.getMFDMaterial.bind(null, request));
});

test('getMFDMaterials does not find any results for an unknown sensor_serial_number', async t => {
    const { request } = t.context;
    request.query.sensor_serial_number = chance.guid();
    await t.throwsAsync(materials.getMFDMaterial.bind(null, request));
});

test('getMFDMaterials does not find any results for an unknown bike_serial_number', async t => {
    const { request } = t.context;
    request.query.bike_serial_number = chance.guid();
    await t.throwsAsync(materials.getMFDMaterial.bind(null, request));
});

test('postMFDMaterialToUser exists', async t => {
    t.truthy(materials.postMFDMaterialToUser);
});

test('postMFDMaterialToUser rejects if no MFDMaterialId is sent', async t => {
    const { request } = t.context;
    delete request.params.id;
    await t.throwsAsync(materials.postMFDMaterialToUser.bind(null, request));
});

test('postMFDMaterialToUser rejects if no user_id is in the query string', async t => {
    const { request } = t.context;
    delete request.query.user_id;
    await t.throwsAsync(materials.postMFDMaterialToUser.bind(null, request));
});

test('getMFDMaterialsForUser exists', async t => {
    t.truthy(materials.getMFDMaterialsForUser);
});

test('getMFDMaterialsForUser rejects if no user_id is in the query string', async t => {
    const { request } = t.context;
    delete request.query.user_id;
    await t.throwsAsync(materials.getMFDMaterialsForUser.bind(null, request));
});

test('deactivateMFDMaterialForUser exists', async t => {
    t.truthy(materials.deactivateMFDMaterialForUser);
});

test('postCustomMFDMaterialToUser exists', async t => {
    t.truthy(materials.postCustomMFDMaterialToUser);
});
