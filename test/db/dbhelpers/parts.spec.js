/**
 * @overview Parts db helpers unit tests
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const test = require('ava');
const chance = require('chance').Chance();
const proxyquire = require('proxyquire');
const dbMock = require('../../../mock/index');

const dbHelpers = proxyquire('../../../db/helpers/parts', {
    '../index': dbMock,
});

test.before(async t => {
    t.context = {
        userId: chance.guid(),
        mfdMaterialId: chance.guid(),
    };
});

test('getMFDParts finds valid row', async t => {
    const q = await dbHelpers.getMFDParts();

    t.truthy(q.length);
    t.truthy(q[0].dataValues.PartId);
    t.truthy(q[0].dataValues.MFDPartId);
    t.truthy(q[0].dataValues.SerialNumber);
});

test('getMFDPartsForUser finds valid row', async t => {
    const q = await dbHelpers.getMFDPartsForUser(
        undefined,
        t.context.userId,
    );
    t.truthy(q.length);
    t.is(q[0].dataValues.UserId, t.context.userId);
    t.is(q[0].dataValues.Active, 1);
});
