/**
 * @overview Materials db helpers unit tests
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const test = require('ava');
const chance = require('chance').Chance();
const proxyquire = require('proxyquire');
const dbMock = require('../../../mock/index');

const dbHelpers = proxyquire('../../../db/helpers/materials', {
    '../index': dbMock,
});

test.before(async t => {
    t.context = {
        userId: chance.guid(),
        mfdMaterialId: chance.guid(),
        userMFDMaterialId: chance.guid(),
        materialId: chance.guid(),
        serialNumber: chance.string(),
    };
});

test('getUserMFDMaterial finds valid row', async t => {
    const q = await dbHelpers.getUserMFDMaterial(
        t.context.userId,
        { mfdMaterialId: t.context.mfdMaterialId },
    );
    t.is(q.dataValues.MFDMaterialId, t.context.mfdMaterialId);
    t.is(q.dataValues.UserId, t.context.userId);
    t.is(q.dataValues.Active, 1);
});

test('getAllUserMFDMaterials finds valid row', async t => {
    const q = await dbHelpers.getAllUserMFDMaterials(
        t.context.userId,
    );
    t.is(q[0].dataValues.UserId, t.context.userId);
});
