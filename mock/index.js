/**
 * @overview sequelize-mock database loader
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const fs = require('fs');
const path = require('path');
const SequelizeMock = require('sequelize-mock');

const dbMock = new SequelizeMock();
const db = {};
const basename = path.basename(__filename);

// Load each mock model definition
fs
    .readdirSync(`${__dirname}/models`)
    .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
    .forEach(file => {
        const model = dbMock.import(path.join(`${__dirname}/models`, file));
        db[model.name] = model;
    });

db.sequelize = dbMock;

module.exports = db;
