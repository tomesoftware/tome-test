/**
 * @overview UserMFDPart mock model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const chance = require('chance').Chance();

module.exports = (mock) => mock.define('UserMFDPart', {
    UserMFDPartId: chance.guid(),
    MFDPartId: chance.guid(),
    UserId: chance.guid(),
    Active: 1,
});
