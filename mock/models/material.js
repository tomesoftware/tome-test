/**
 * @overview Material mock model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const chance = require('chance').Chance();

module.exports = (mock) => mock.define('Material', {
    MaterialId: chance.guid(),
    Active: 1,
});
