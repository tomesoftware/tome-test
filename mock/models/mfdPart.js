/**
 * @overview MFDPart mock model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const chance = require('chance').Chance();

module.exports = (mock) => mock.define('MFDPart', {
    MFDPartId: chance.guid(),
    PartId: chance.guid(),
    SerialNumber: chance.string(),
    Active: 1,
});
