/**
 * @overview MFDMaterial mock model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const chance = require('chance').Chance();

module.exports = (mock) => mock.define('MFDMaterial', {
    MFDMaterialId: chance.guid(),
    MaterialId: chance.guid(),
    SerialNumber: chance.string(),
    Active: 1,
});
