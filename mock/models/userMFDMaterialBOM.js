/**
 * @overview UserMFDMaterialBOM mock model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const chance = require('chance').Chance();

module.exports = (mock) => mock.define('UserMFDMaterialBOM', {
    UserId: chance.guid(),
    MFDMaterialId: chance.guid(),
    MFDPartId: chance.guid(),
    Active: 1,
});
