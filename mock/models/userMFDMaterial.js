/**
 * @overview UserMFDMaterial mock model
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const chance = require('chance').Chance();

module.exports = (mock) => mock.define('UserMFDMaterial', {
    UserMFDMaterialId: chance.guid(),
    UserId: chance.guid(),
    MFDMaterialId: chance.guid(),
    SerialNumber: chance.string(),
    Active: 1,
});
