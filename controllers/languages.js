/**
 * @overview Handles requests related to languages
 * @author Nick Dedenbach
 * @copyright Tome Software 2019
 */

'use strict';

const Boom = require('boom');
const dbHelpers = require('../db/helpers/languages');

const languageModel = require('../models/language');

/**
 * Get a resource language by ID
 */
const getLanguage = async (request, h) => {
    try {
        const language = await dbHelpers.getLanguage(request.params.language_id);

        return h.response(languageModel.serialize.databaseToApi(language)).code(200);
    } catch (e) {
        if (e.isBoom) {
            throw e;
        }

        throw Boom.badImplementation(e.message);
    }
};

/**
 * Get a full list of supported resource languages
 */
const getLanguages = async (request, h) => {
    try {
        const languages = await dbHelpers.getLanguages();

        return h.response(languages.map(languageModel.serialize.databaseToApi)).code(200);
    } catch (e) {
        if (e.isBoom) {
            throw e;
        }

        throw Boom.badImplementation(e.message);
    }
};

module.exports = {
    getLanguage,
    getLanguages,
};
