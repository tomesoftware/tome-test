/**
 * @overview Handles requests related to maintenances
 * @author Matt Rathbun
 * @copyright Tome Software 2018
 */

'use strict';

const Boom = require('boom');
const dbHelpers = require('../db/helpers/maintenances');

const materialMaintenanceModel = require('../models/userMFDMaterialMaintenance');
const partMaintenanceModel = require('../models/userMFDPartMaintenance');

/**
 * Get a list of maintenances for a given UserMFDMaterial.
 */
const getMaintenances = async (request, h) => {
    try {
        const optionals = {
            includePerformed: request.query.include_performed,
        };

        const maintenances = await dbHelpers.getMaintenances(
            request.query.user_id,
            request.query.user_mfd_material_id,
            optionals,
        ).map(eachUserMFDMaterial => ({
            Model: eachUserMFDMaterial.MFDMaterial.Material.Model,
            Parts: eachUserMFDMaterial.UserMFDMaterialBOMs
                .map(eachUserMFDMaterialBOM => eachUserMFDMaterialBOM.UserMFDPart.MFDPart.Part),
        }));

        return h.response(maintenances).code(200);
    } catch (e) {
        if (e.isBoom) {
            throw e;
        }

        throw Boom.badImplementation(e.message);
    }
};

/**
 * Create a new UserMFDMaterialMaintenance
 */
const postMaterialMaintenances = async (request, h) => {
    try {
        const newMaintenance = await dbHelpers.createUserMFDMaterialMaintenance(
            request.payload.user_mfd_material_id,
            request.payload.model_maintenance_id,
            {
                serviceDate: request.payload.service_date,
                notes: request.payload.notes,
                dealerId: request.payload.dealer_id,
                odometer: request.payload.odometer,
            },
        );
        return h.response(materialMaintenanceModel.serialize.databaseToApi(newMaintenance)).code(200);
    } catch (e) {
        if (e.isBoom) {
            throw e;
        }

        throw Boom.badImplementation(e.message);
    }
};

/**
 * Create a new UserMFDPartMaintenance
 */
const postPartMaintenances = async (request, h) => {
    try {
        const newMaintenance = await dbHelpers.createUserMFDPartMaintenance(
            request.payload.user_mfd_part_id,
            request.payload.part_maintenance_id,
            {
                serviceDate: request.payload.service_date,
                notes: request.payload.notes,
                dealerId: request.payload.dealer_id,
                odometer: request.payload.odometer,
            },
        );
        return h.response(partMaintenanceModel.serialize.databaseToApi(newMaintenance)).code(200);
    } catch (e) {
        if (e.isBoom) {
            throw e;
        }

        throw Boom.badImplementation(e.message);
    }
};

module.exports = {
    getMaintenances,
    postMaterialMaintenances,
    postPartMaintenances,
};
