/**
 * @overview Handles requests related to parts
 * @author Ben Willshire
 * @copyright Tome Software 2018
 */

'use strict';

const Boom = require('boom');

const dbHelpers = require('../db/helpers');
const mfdPartModel = require('../models/mfdPart');
const userMFDPartModel = require('../models/userMfdPart');
const userMFDMaterialModel = require('../models/userMfdMaterial');

/**
 * Retrieve an Array of MFDParts (usually just one result) that matches the provided
 * user_id or part_type from the query string.
 */
const getMFDParts = async (request, h) => {
    try {
        let dbParts = {};
        if (request.query.user_id) {
            dbParts = await dbHelpers.parts.getMFDPartsForUser(request.query.part_type, request.query.user_id);
            dbParts = dbParts.map(part => part.MFDPart);
        } else {
            dbParts = await dbHelpers.parts.getMFDParts({ partType: request.query.part_type });
        }
        const parts = dbParts.map(part => mfdPartModel.serialize.databaseToApi(part));
        return h.response(parts).code(200);
    } catch (e) {
        if (e.isJoi) {
            throw Boom.badRequest(e.details[0].message);
        } else if (e.isBoom) {
            throw e;
        }
        throw Boom.badImplementation(e.message);
    }
};

const putUserMFDParts = async (request, h) => {
    const userMFDPartId = request.params.id;
    const userId = request.query.user_id;
    const userMFDPartAttributes = request.payload.user_mfd_part_attributes || [];
    try {
        const alreadyCreatedAttributes = userMFDPartAttributes.filter(a => a.user_mfd_part_attribute_id);
        await Promise.all(alreadyCreatedAttributes.map(attribute => dbHelpers.attributes.putUserMFDPartAttributes(attribute, userId)));
        const notCreatedAttributes = userMFDPartAttributes.filter(a => !a.user_mfd_part_attribute_id);
        await dbHelpers.parts.createUserMFDPartAttributes({ userMFDPartId }, notCreatedAttributes, userId);

        const userMFDPart = await dbHelpers.parts.getMFDPartsForUser(null, userId, userMFDPartId, true);
        const validatedUserMFDPart = userMFDPartModel.serialize.databaseToApi(userMFDPart);
        return h.response(validatedUserMFDPart).code(200);
    } catch (e) {
        if (e.isJoi) {
            throw Boom.badRequest(e.details[0].message);
        } else if (e.isBoom) {
            throw e;
        }
        throw Boom.badImplementation(e.message);
    }
};

const postUserMFDParts = async (request, h) => {
    const serialNumber = request.payload.serial_number;
    const partType = request.payload.part_type;
    const mfdMaterialId = request.payload.mfd_material_id;
    const userId = request.query.user_id;
    const userMFDPartAttributes = request.payload.user_mfd_part_attributes || [];
    const includeAttributes = request.query.include_attributes;

    let mfdParts = await dbHelpers.parts.getMFDParts({
        serialNumber: serialNumber,
        partType: partType,
    });

    if (mfdParts.length === 0) {
        const mfdPart = await dbHelpers.parts.createMFDPart(
            'Speed Sensor',
            serialNumber,
            null,
            {
                userId: userId,
            },
        );
        mfdParts = [mfdPart];
    }

    const mfdPartId = mfdParts[0].dataValues.MFDPartId;
    let userMFDPart;
    try {
        userMFDPart = await dbHelpers.parts.createUserMFDPart(
            userId,
            mfdPartId,
            null,
            {
                name: 'Speed Sensor',
            },
        );
    } catch (e) {
        if (e.message && e.message.startsWith('Violation of PRIMARY KEY constraint')) {
            throw Boom.badRequest('Sensor is already paired to this bike');
        } else {
            throw Boom.badImplementation(e.message);
        }
    }

    const userMFDMaterial = await dbHelpers.materials.getUserMFDMaterial(userId, { mfdMaterialId }, true);
    const userMFDMaterialId = userMFDMaterial.dataValues.UserMFDMaterialId;

    await dbHelpers.materials.associateUserWithMFDMaterialBOM(userMFDPart.dataValues.UserMFDPartId, userMFDMaterialId, userId);

    await dbHelpers.parts.createUserMFDPartAttributes({ userMFDPartId: userMFDPart.dataValues.UserMFDPartId }, userMFDPartAttributes, request.query.user_id);

    userMFDMaterial.UserMFDParts = await dbHelpers.materials.getUserMFDMaterialBOM(userMFDMaterialId, false, includeAttributes);
    const validatedUserMFDMaterial = userMFDMaterialModel.serialize.databaseToApi(userMFDMaterial);
    return h.response(validatedUserMFDMaterial).code(200);
};

module.exports = {
    getMFDParts,
    putUserMFDParts,
    postUserMFDParts,
};
