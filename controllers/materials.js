/**
 * @overview Handles requests related to materials
 * @author Mike Swierenga
 * @copyright Tome Software 2018
 */

'use strict';

const Boom = require('boom');

const db = require('../db');
const dbHelpers = require('../db/helpers');
const mfdMaterialModel = require('../models/mfdMaterial');
const mfdPartModel = require('../models/mfdPart');
const userMFDMaterialModel = require('../models/userMfdMaterial');
const wheelSizeModel = require('../models/wheelSize');
const statusModel = require('../models/status');
const manufacturersModel = require('../models/manufacturer');

const TIRE_CATEGORY_NAME = 'Tire';

/**
 * Retrieve an Array of MFDMaterials (usually just one result) that matches the provided
 * sensor_serial_number or bike_serial_number from the query string.
 *
 * Params guaranteed to be validated by route:
 *  - request.query
 *      - (string|undefined) sensor_serial_number
 *      - (string|undefined) bike_serial_number
 *      - (boolean|undefined) include_resources
 *      - (boolean|undefined) include_parts
 */
const getMFDMaterial = async (request, h) => {
    const sensorSerialNumber = request.query.sensor_serial_number;
    const bikeSerialNumber = request.query.bike_serial_number;
    const includeResources = request.query.include_resources;
    const includeParts = request.query.include_parts;
    const includeAttributes = request.query.include_attributes;
    const includeUserCreated = request.query.include_user_created || false;

    let results;

    if (sensorSerialNumber) {
        // Try to look up by sensor serial number
        const mfdMaterialBOMs = await dbHelpers.materials.getMFDMaterialBOMFromPartSerialNumber(
            sensorSerialNumber, includeResources, includeAttributes,
        );

        // Ensure we found at least one matching mfd material
        if (mfdMaterialBOMs.length === 0) {
            throw Boom.notFound(`No manufactured material with sensor_serial_number=${sensorSerialNumber} was found.`);
        }

        // Convert the results to the response JSON
        try {
            const validatedResults = [];
            mfdMaterialBOMs.forEach(bom => {
                validatedResults.push(mfdMaterialModel.serialize.databaseToApi(bom.MFDMaterial));
            });
            results = validatedResults;
        } catch (e) {
            throw Boom.badImplementation(e);
        }
    } else if (bikeSerialNumber) {
        // Try to look up by bike serial number
        const mfdMaterials = await dbHelpers.materials.getMFDMaterialFromBikeSerialNumber(
            bikeSerialNumber, includeResources, includeAttributes, !includeUserCreated,
        );

        // Ensure we found at least one matching mfd material
        if (mfdMaterials.length === 0) {
            throw Boom.notFound(`No manufactured material with bike_serial_number=${bikeSerialNumber} was found.`);
        }

        // Convert the results to the response JSON
        try {
            const validatedResults = [];
            mfdMaterials.forEach(mfdMaterial => {
                validatedResults.push(mfdMaterialModel.serialize.databaseToApi(mfdMaterial));
            });
            results = validatedResults;
        } catch (e) {
            throw Boom.badImplementation(e);
        }
    } else {
        throw Boom.badRequest(
            'Either sensor_serial_number or bike_serial_number param is required.',
        );
    }

    if (includeParts) {
        // Retrieve all mfd_parts for each mfd_material found
        const asyncGetMFDParts = (mfdMaterial) => dbHelpers.materials.getMFDPartsForMFDMaterial(
            mfdMaterial.mfd_material_id, includeResources, includeAttributes,
        );
        const allMfdPartsResults = await Promise.all(results.map(asyncGetMFDParts));
        for (let i = 0; i < results.length; i++) {
            results[i].mfd_parts = [];
            allMfdPartsResults[i].forEach(item => {
                results[i].mfd_parts.push(mfdPartModel.serialize.databaseToApi(item.MFDPart));
            });
        }
    }

    return h.response(results).code(200);
};

const getNameByMFDMaterialId = async (request, h) => {
    const mfdMaterialId = request.params.mfd_material_id;
    const userId = request.query.user_id;

    const userMFDMaterial = await dbHelpers.materials.getUserMFDMaterial(userId, { mfdMaterialId });
    if (userMFDMaterial && userMFDMaterial.dataValues.Name) {
        return h.response({ name: userMFDMaterial.dataValues.Name, mfd_material_id: mfdMaterialId }).code(200);
    }
    // Try to look up by id
    const mfdMaterial = await dbHelpers.materials.getMFDMaterialById(mfdMaterialId);

    // Ensure we found matching mfd material
    if (!mfdMaterial) {
        throw Boom.notFound(`No manufactured material with mfd_material_id=${mfdMaterialId} was found.`);
    }
    return h.response({ name: mfdMaterial.Material.dataValues.Name, mfd_material_id: mfdMaterialId }).code(200);
};

/**
 * Trigger a confirmation that the logged in user is the owner of the
 * MFDMaterial with MFDMaterialId contained in the request path.
 *
 * Links the user to the MFDMaterial by creating an entry in the
 * UserMFDMaterialBOM table.
 *
 * Links each MFDPart associated with the user by creating an entry for
 * each in the UserMFDPart table.
 *
 * Links each UserMFDPart entry to a UserMFDMaterial entry in the
 * UserMFDMaterialBOM table.
 *
 * Params guaranteed to be validated by route:
 *  - request.params
 *      - (uuid) mfd_material_id : MFDMaterialId
 *  - request.query
 *      - (uuid) user_id : UserId
 */
const postMFDMaterialToUser = async (request, h) => {
    const mfdMaterialId = request.params.mfd_material_id;
    const customUserMFDPartAttributes = request.payload.user_mfd_part_attributes || [];
    const userId = request.query.user_id;
    const includeAttributes = request.query.include_attributes;
    const includeSetup = request.query.include_setup;
    const sensorSerialNumber = request.payload.sensor_serial_number;
    const userMFDPartAttributes = [];

    // Associate the provided userId with the mfdMaterialId and all of its associated mfdParts
    let associationResult;
    try {
        associationResult = await dbHelpers.materials.associateUserWithMFDMaterial(userId, mfdMaterialId);
    } catch (e) {
        console.log(e);
        throw Boom.badRequest(e.message);
    }

    if (!associationResult.user_mfd_material.newly_created) {
        // If there was already an entry (and it is inactive), set it and its
        // parts to active
        if (associationResult.user_mfd_material.dataValues.Active === false) {
            const transaction = await db.sequelize.transaction();
            try {
                await dbHelpers.materials.setActivationForUserMFDMaterial(
                    true,
                    associationResult.user_mfd_material.dataValues.UserMFDMaterialId,
                    null,
                );
                await transaction.commit();
            } catch (e) {
                await transaction.rollback();
                throw Boom.badImplementation(e.message);
            }
        } else {
            throw Boom.badRequest(`This user is already associated with mfd_material_id = ${mfdMaterialId}`);
        }
    }

    try {
        const userMFDMaterial = await dbHelpers.materials.getUserMFDMaterial(userId, { mfdMaterialId }, true);

        let sensorMFDPartID;
        // Add a sensor by serial number if they passed it, or get one from a BOM
        if (sensorSerialNumber) {
            const userMFDMaterialId = userMFDMaterial.dataValues.UserMFDMaterialId;
            let mfdPart = await dbHelpers.parts.getMFDParts({ serialNumber: sensorSerialNumber, partName: 'Speed Sensor' });
            if (mfdPart.length === 0) {
                const mfdPartNew = await dbHelpers.parts.createMFDPart(
                    'Speed Sensor',
                    sensorSerialNumber,
                    null,
                    {
                        userId: userId,
                    },
                );
                mfdPart = [mfdPartNew];
            }
            sensorMFDPartID = mfdPart[0].dataValues.MFDPartId;
            const userMFDPart = await dbHelpers.materials.associateUserWithMFDPart(request.query.user_id, sensorMFDPartID, request.query.user_id);
            await dbHelpers.materials.associateUserWithMFDMaterialBOM(userMFDPart.dataValues.UserMFDPartId, userMFDMaterialId, request.query.user_id);
        }

        if (sensorMFDPartID) {
            // Set passed attributes to apply to the created part
            customUserMFDPartAttributes.forEach(attr => {
                userMFDPartAttributes.push(Object.assign({ mfd_part_id: sensorMFDPartID }, attr));
            });
        }
        // find sensors in the UserMFDMaterialBOM and set tire attrs on it
        const userMFDMaterialBOM = await dbHelpers.materials.getUserMFDMaterialBOM(userMFDMaterial.dataValues.UserMFDMaterialId);
        const sensors = userMFDMaterialBOM.filter(bom => bom.UserMFDPart.MFDPart.Part.PartType.dataValues.Name === 'Speed Sensor');
        sensors.forEach(sensor => {
            const eachSensorMFDPartID = sensor.UserMFDPart.MFDPart.dataValues.MFDPartId;
            // copy tire attributes from material
            userMFDMaterial.MFDMaterial.Material.MaterialAttributes.forEach(materialAttribute => {
                const categoryName = materialAttribute.Attribute.AttributeCategory.dataValues.Name;
                if (categoryName === TIRE_CATEGORY_NAME) {
                    userMFDPartAttributes.push({
                        mfd_part_id: eachSensorMFDPartID,
                        name: materialAttribute.Attribute.dataValues.Name,
                        category: categoryName,
                        value: materialAttribute.dataValues.Value,
                    });
                }
            });
        });


        await dbHelpers.parts.createUserMFDPartAttributes({ userMFDMaterialBOM }, userMFDPartAttributes, userId);

        // get list of userMFDParts for the bike
        userMFDMaterial.UserMFDParts = await dbHelpers.materials.getUserMFDMaterialBOM(userMFDMaterial.dataValues.UserMFDMaterialId, false, includeAttributes, includeSetup);

        const validatedUserMFDMaterial = userMFDMaterialModel.serialize.databaseToApi(userMFDMaterial);
        return h.response(validatedUserMFDMaterial).code(201);
    } catch (e) {
        if (e.isJoi) {
            throw Boom.badImplementation(e.details[0].message);
        } else if (e.isBoom) {
            throw e;
        }
        throw Boom.badImplementation(e.message);
    }
};

/**
 * Create new MFDMaterial entity for a user and add it to a
 * UserMFDMaterial entity. Creates and associates with
 * Manufacturer, Platform, Model, & Material as needed.
 *
 * Params guaranteed to be validated by route:
 *  - request.query
 *      - (uuid) user_id : UserId
 *  - request.payload
 *      - (string) serial_number
 *      - (string|undefined) manufacturer
 *      - (string|undefined) model
 *      - (string|undefined) color
 *      - (string|undefined) bike_size
 *      - (string|undefined) sensor_serial_number
 */
const postCustomMFDMaterialToUser = async (request, h) => {
    const serialNumber = request.payload.serial_number;
    const manufacturerName = request.payload.manufacturer;
    const modelName = request.payload.model;
    const { color } = request.payload;
    const bikeSize = request.payload.bike_size;
    const platformType = request.payload.platform_type;
    const modelYear = request.payload.model_year;
    const sensorSerialNumber = request.payload.sensor_serial_number;
    const userMFDPartAttributes = request.payload.user_mfd_part_attributes || [];
    const userId = request.query.user_id;

    const createdEntities = {};
    const transaction = await db.sequelize.transaction();
    let mfdMaterialId = null;

    try {
        let manufacturerId = null;
        let platformId = null;

        if (manufacturerName) {
            [createdEntities.manufacturer] = await dbHelpers.manufacturers.findOrCreateManufacturer(
                transaction,
                {
                    name: manufacturerName,
                    createdBy: userId,
                },
            );

            manufacturerId = createdEntities.manufacturer.dataValues.ManufacturerId;

            // If we create a Manufacturer, then we need to create a Platform, Model,
            // and Material too, so the Manufacturer can be linked down to the MFDMaterial
            createdEntities.platformType = await dbHelpers.platforms.createPlatformType(
                transaction,
                {
                    name: platformType,
                    createdBy: userId,
                },
            );

            const platformTypeId = createdEntities.platformType.dataValues.PlatformTypeId;
            createdEntities.platform = await dbHelpers.platforms.createPlatform(
                transaction,
                {
                    name: modelName,
                    manufacturerId: manufacturerId,
                    platformTypeId: platformTypeId,
                    createdBy: userId,
                },
            );
            platformId = createdEntities.platform.dataValues.PlatformId;
        }


        createdEntities.model = await dbHelpers.models.createModel(
            transaction,
            {
                name: modelName, // use model name as platform name
                platformId: platformId,
                createdBy: userId,
                modelYear: modelYear,
            },
        );

        const modelId = createdEntities.model.dataValues.ModelId;

        createdEntities.material = await dbHelpers.materials.createMaterial(
            modelName, // use model name as material name
            transaction,
            {
                modelId: modelId,
                color: color,
                frameSize: bikeSize,
                createdBy: userId,
            },
        );

        const materialId = createdEntities.material.dataValues.MaterialId;

        createdEntities.mfd_material = await dbHelpers.materials.createMFDMaterial(
            transaction,
            {
                createdBy: userId,
                serialNumber: serialNumber,
                materialId: materialId,
            },
        );

        mfdMaterialId = createdEntities.mfd_material.dataValues.MFDMaterialId;

        const statusIdQuery = await db.Status.find({
            where: {
                Name: 'Active',
            },
            attributes: ['StatusId'],
        });
        if (!statusIdQuery) {
            throw new Boom('Could not find Active status in db');
        }
        const activeStatusId = statusIdQuery.dataValues.StatusId;

        createdEntities.user_mfd_material = await dbHelpers.materials.createUserMFDMaterial(
            userId,
            mfdMaterialId,
            activeStatusId,
            transaction,
        );

        if (sensorSerialNumber) {
            const userMFDMaterialId = createdEntities.user_mfd_material.dataValues.UserMFDMaterialId;
            let mfdPart = await dbHelpers.parts.getMFDParts({ serialNumber: sensorSerialNumber, partName: 'Speed Sensor' });
            if (mfdPart.length === 0) {
                const mfdPartNew = await dbHelpers.parts.createMFDPart(
                    'Speed Sensor',
                    sensorSerialNumber,
                    null,
                    {
                        userId: userId,
                    },
                );
                mfdPart = [mfdPartNew];
            }
            const mfdPartId = mfdPart[0].dataValues.MFDPartId;
            const userMFDPart = await dbHelpers.materials.associateUserWithMFDPart(userId, mfdPartId, userId, transaction);
            await dbHelpers.materials.associateUserWithMFDMaterialBOM(userMFDPart.dataValues.UserMFDPartId, userMFDMaterialId, userId, transaction);
            userMFDPartAttributes.forEach(attribute => {
                attribute.mfd_part_id = mfdPartId;
            });
            await dbHelpers.parts.createUserMFDPartAttributes({ userMFDPartId: userMFDPart.dataValues.UserMFDPartId }, userMFDPartAttributes, userId, transaction);
        }
        await transaction.commit();
    } catch (e) {
        await transaction.rollback();
        console.log(e);
        throw Boom.badImplementation(e.message);
    }

    try {
        const dbUserMFDMaterial = await dbHelpers.materials.getUserMFDMaterial(
            userId,
            { mfdMaterialId },
        );

        const validatedUserMFDMaterial = userMFDMaterialModel.serialize.databaseToApi(dbUserMFDMaterial);

        return h.response(validatedUserMFDMaterial).code(201);
    } catch (e) {
        console.log(e);
        throw Boom.badImplementation(e.message);
    }
};

/**
 * Retrieve an Array of UserMFDMaterials that are associated with a
 * given user.
 *
 * Params guaranteed to be validated by route:
 *  - request.query
 *      - (uuid) user_id : UserId
 *      - (boolean|undefined) include_resources
 *      - (boolean|undefined) include_parts
 */
const getMFDMaterialsForUser = async (request, h) => {
    const userId = request.query.user_id;
    const includeResources = request.query.include_resources;
    const includeParts = request.query.include_parts;
    const includeAttributes = request.query.include_attributes;
    const includeSetup = request.query.include_setup;

    const userMFDMaterials = await dbHelpers.materials.getAllUserMFDMaterials(
        userId, includeResources, includeAttributes,
    ).catch((e) => {
        console.log('got a db failure on /materials/mfd/user');
        console.log(e);
        throw e;
    });

    const validatedResults = [];
    // Get all associated parts, if specified
    if (includeParts) {
        // Get all the user-specific parts for user-specific material
        const getUserMFDMaterialBOM = (userMFDMat) => dbHelpers.materials.getUserMFDMaterialBOM(
            userMFDMat.dataValues.UserMFDMaterialId, includeResources, includeAttributes, includeSetup,
        );

        const userMFDMaterialBOMs = await Promise.all(userMFDMaterials.map(getUserMFDMaterialBOM));

        // Get all the generic parts for the generic material
        const getMaterialBOM = (material) => dbHelpers.materials.getMaterialBOM(
            material.dataValues.MaterialId, includeResources, includeAttributes, includeSetup,
        );
        const materialBOMs = await Promise.all(userMFDMaterials.map(
            userMFDMat => getMaterialBOM(userMFDMat.MFDMaterial.Material),
        ));

        for (let i = 0; i < userMFDMaterials.length; i++) {
            userMFDMaterials[i].UserMFDParts = userMFDMaterialBOMs[i];
            userMFDMaterials[i].Parts = materialBOMs[i];
            validatedResults.push(
                userMFDMaterialModel.serialize.databaseToApi(userMFDMaterials[i]),
            );
        }
    } else {
        userMFDMaterials.forEach(mat => {
            validatedResults.push(userMFDMaterialModel.serialize.databaseToApi(mat));
        });
    }
    return h.response(validatedResults).code(200);
};

const getWheelSizes = async (request, h) => {
    const wheelSizes = await dbHelpers.materials.getWheelSizes();
    const wheelSizeAPI = wheelSizes.map((wheelSize) => wheelSizeModel.serialize.databaseToApi(wheelSize.dataValues));
    wheelSizeAPI.forEach(wheelSize => {
        try {
            wheelSizeModel.validate(wheelSize);
        } catch (e) {
            throw Boom.badImplementation(`DB returned invalid wheelsize: ${e.message}`);
        }
    });
    return h.response({ wheel_sizes: wheelSizeAPI }).code(200);
};

const getStatuses = async (request, h) => {
    try {
        const statuses = await dbHelpers.materials.getStatuses();
        const statusAPI = statuses.map((status) => statusModel.serialize.databaseToApi(status.dataValues));
        return h.response({ statuses: statusAPI }).code(200);
    } catch (e) {
        console.log('failed to get statuses /materials/statuses');
        console.log(e);
        throw e;
    }
};

/**
 * Deactivate (set active=false) an association of a MFDMaterial
 * with a user. This includes all of the MFDParts that are linked
 * in the UserMFDMaterialBOM.
 *
 * Params guaranteed to be validated by route:
 *  - request.params
 *      - (uuid) user_mfd_material_id : UserMFDMaterialId
 *  - request.query
 *      - (uuid) user_id : UserId
 */
const deactivateMFDMaterialForUser = async (request, h) => {
    const userMFDMaterialId = request.params.user_mfd_material_id;
    const transaction = await db.sequelize.transaction();
    try {
        // deactivate the UserMFDMaterial, UserMFDMaterialBOM, and any UserMFDParts that aren't in other UserMFDMaterialBOM entries for other UserMFDMaterials
        const deactivation = await dbHelpers.materials.setActivationForUserMFDMaterial(
            false,
            userMFDMaterialId,
            transaction,
        );
        await transaction.commit();
        return h.response({ deactivated: deactivation }).code(200);
    } catch (e) {
        await transaction.rollback();
        console.log(e);
        if (e.isBoom) {
            throw e;
        }
        throw Boom.badImplementation(e.message);
    }
};

const putUserMFDMaterials = async (request, h) => {
    const userMFDMaterialId = request.params.user_mfd_material_id;
    const userId = request.query.user_id;
    const userMFDMaterialAttributes = request.payload.user_mfd_material_attributes || [];
    const { name } = request.payload;
    const statusId = request.payload.status_id;
    let userMFDMaterial;
    try {
        if (name || statusId) {
            userMFDMaterial = await dbHelpers.materials.updateUserMFDMaterial(userMFDMaterialId, userId, name, statusId);
        }
        const alreadyCreatedAttributes = userMFDMaterialAttributes.filter(a => a.user_mfd_material_attribute_id);
        await Promise.all(alreadyCreatedAttributes.map(attribute => dbHelpers.attributes.putUserMFDMaterialAttributes(attribute, userId)));
        const notCreatedAttributes = userMFDMaterialAttributes.filter(a => !a.user_mfd_material_attribute_id);
        await dbHelpers.materials.createUserMFDMaterialAttributes(userMFDMaterialId, notCreatedAttributes, userId);

        userMFDMaterial = await dbHelpers.materials.getUserMFDMaterial(userId, { userMFDMaterialId }, true);
        userMFDMaterial.UserMFDParts = await dbHelpers.materials.getUserMFDMaterialBOM(userMFDMaterialId, false, true);
        const validatedUserMFDMaterial = userMFDMaterialModel.serialize.databaseToApi(userMFDMaterial);
        return h.response(validatedUserMFDMaterial).code(200);
    } catch (e) {
        if (e.isJoi) {
            throw Boom.badRequest(e.details[0].message);
        } else if (e.isBoom) {
            throw e;
        }
        throw Boom.badImplementation(e.message);
    }
};

const getManufacturers = async (request, h) => {
    try {
        const manufacturers = await dbHelpers.manufacturers.getManufacturers();
        return h.response({ manufacturers: manufacturers.map((m) => manufacturersModel.serialize.databaseToApi(m)) }).code(200);
    } catch (e) {
        console.log(e);
        throw Boom.badImplementation(e.message);
    }
};
module.exports = {
    getMFDMaterial,
    getNameByMFDMaterialId,
    getMFDMaterialsForUser,
    getWheelSizes,
    getStatuses,
    postMFDMaterialToUser,
    postCustomMFDMaterialToUser,
    deactivateMFDMaterialForUser,
    putUserMFDMaterials,
    getManufacturers,
};
